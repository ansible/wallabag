Example of a `make install` run using Ansible task.

```
TASK [wallabag : Run Wallabag 'make install'] *************************************************************************************

composer.phar not found, we'll see if composer is installed globally.
HEAD is now at 919d7da5 Merge pull request #5155 from wallabag/release/2.4.2
Installing dependencies from lock file
Verifying lock file contents can be installed on current platform.
Package operations: 162 installs, 0 updates, 0 removals
    0 [>---------------------------]    0 [>---------------------------]    0 [>---------------------------]  
  - Installing composer/package-versions-deprecated (1.11.99.1): Extracting archive
  - Installing symfony/polyfill-mbstring (v1.22.1): Extracting archive
  - Installing symfony/polyfill-ctype (v1.22.1): Extracting archive
  - Installing twig/twig (v2.14.4): Extracting archive
  - Installing symfony/polyfill-php70 (v1.20.0)
  - Installing symfony/polyfill-php56 (v1.20.0)
  - Installing symfony/polyfill-intl-icu (v1.22.1): Extracting archive
  - Installing symfony/polyfill-apcu (v1.22.1): Extracting archive
  - Installing psr/simple-cache (1.0.1): Extracting archive
  - Installing psr/log (1.1.3): Extracting archive
  - Installing psr/link (1.0.0): Extracting archive
  - Installing psr/container (1.1.1): Extracting archive
  - Installing psr/cache (1.0.1): Extracting archive
  - Installing fig/link-util (1.1.2): Extracting archive
  - Installing doctrine/lexer (1.2.1): Extracting archive
  - Installing doctrine/annotations (1.12.1): Extracting archive
  - Installing doctrine/reflection (1.2.2): Extracting archive
  - Installing doctrine/event-manager (1.1.1): Extracting archive
  - Installing doctrine/collections (1.6.7): Extracting archive
  - Installing doctrine/cache (1.10.2): Extracting archive
  - Installing doctrine/persistence (1.3.8): Extracting archive
  - Installing doctrine/inflector (1.4.3): Extracting archive
  - Installing doctrine/common (2.13.3): Extracting archive
  - Installing symfony/symfony (v3.4.47): Extracting archive
  - Installing symfony/deprecation-contracts (v2.2.0): Extracting archive
  - Installing pagerfanta/pagerfanta (v2.7.1): Extracting archive
  - Installing babdev/pagerfanta-bundle (v2.9.0): Extracting archive
  - Installing react/promise (v2.8.0): Extracting archive
  - Installing guzzlehttp/streams (3.0.0): Extracting archive
  - Installing guzzlehttp/ringphp (1.1.1): Extracting archive
  - Installing guzzlehttp/guzzle (5.3.4): Extracting archive
  - Installing bdunogier/guzzle-site-authenticator (1.0.0): Extracting archive
  - Installing behat/transliterator (v1.3.0): Extracting archive
  - Installing clue/stream-filter (v1.5.0): Extracting archive
  - Installing symfony/service-contracts (v2.2.0): Extracting archive
  - Installing jdorn/sql-formatter (v1.2.17): Extracting archive
  - Installing doctrine/doctrine-cache-bundle (1.4.0): Extracting archive
  - Installing doctrine/dbal (2.10.4): Extracting archive
  - Installing doctrine/doctrine-bundle (1.12.13): Extracting archive
  - Installing craue/config-bundle (2.4.0): Extracting archive
  - Installing paragonie/random_compat (v2.0.19): Extracting archive
  - Installing defuse/php-encryption (v2.2.1): Extracting archive
  - Installing laminas/laminas-zendframework-bridge (1.1.1): Extracting archive
  - Installing laminas/laminas-eventmanager (3.2.1): Extracting archive
  - Installing laminas/laminas-code (3.4.1): Extracting archive
  - Installing ocramius/proxy-manager (2.2.3): Extracting archive
  - Installing doctrine/migrations (v1.8.1): Extracting archive
  - Installing doctrine/doctrine-migrations-bundle (v1.3.2): Extracting archive
  - Installing symfony/polyfill-php72 (v1.22.1): Extracting archive
  - Installing symfony/polyfill-intl-normalizer (v1.22.1): Extracting archive
  - Installing symfony/polyfill-intl-idn (v1.22.1): Extracting archive
  - Installing egulias/email-validator (3.1.0): Extracting archive
  - Installing willdurand/jsonp-callback-validator (v1.1.0): Extracting archive
  - Installing friendsofsymfony/jsrouting-bundle (2.7.0): Extracting archive
  - Installing friendsofsymfony/oauth2-php (1.3.0): Extracting archive
  - Installing friendsofsymfony/oauth-server-bundle (1.6.2): Extracting archive
  - Installing willdurand/negotiation (3.0.0): Extracting archive
  - Installing friendsofsymfony/rest-bundle (2.8.6): Extracting archive
  - Installing friendsofsymfony/user-bundle (v2.0.2): Extracting archive
  - Installing grandt/relativepath (1.0.2): Extracting archive
  - Installing grandt/binstring (1.0.0): Extracting archive
  - Installing grandt/phpzipmerge (1.0.4): Extracting archive
  - Installing hoa/exception (1.17.01.16): Extracting archive
  - Installing hoa/consistency (1.17.05.02): Extracting archive
  - Installing hoa/event (1.17.01.13): Extracting archive
  - Installing hoa/iterator (2.17.01.10): Extracting archive
  - Installing hoa/visitor (2.17.01.16): Extracting archive
  - Installing hoa/ustring (4.17.01.16): Extracting archive
  - Installing hoa/protocol (1.17.01.14): Extracting archive
  - Installing hoa/zformat (1.17.01.10): Extracting archive
  - Installing hoa/regex (1.17.01.13): Extracting archive
  - Installing hoa/math (1.17.05.16): Extracting archive
  - Installing hoa/stream (1.17.02.21): Extracting archive
  - Installing hoa/file (1.17.07.11): Extracting archive
  - Installing hoa/compiler (3.17.08.08): Extracting archive
  - Installing hoa/ruler (2.17.05.16): Extracting archive
  - Installing html2text/html2text (4.3.1): Extracting archive
  - Installing incenteev/composer-parameter-handler (v2.1.4): Extracting archive
  - Installing true/punycode (v2.1.1): Extracting archive
  - Installing smalot/pdfparser (v0.18.2): Extracting archive
  - Installing simplepie/simplepie (1.5.6): Extracting archive
  - Installing psr/http-message (1.0.1): Extracting archive
  - Installing php-http/message-factory (v1.0.2): Extracting archive
  - Installing php-http/message (1.11.0): Extracting archive
  - Installing psr/http-client (1.0.1): Extracting archive
  - Installing php-http/promise (1.1.0): Extracting archive
  - Installing php-http/httplug (2.2.0): Extracting archive
  - Installing php-http/discovery (1.13.0): Extracting archive
  - Installing symfony/polyfill-php80 (v1.22.1): Extracting archive
  - Installing psr/http-factory (1.0.1): Extracting archive
  - Installing php-http/client-common (2.3.0): Extracting archive
  - Installing monolog/monolog (1.26.0): Extracting archive
  - Installing masterminds/html5 (2.7.4): Extracting archive
  - Installing j0k3r/php-readability (1.2.7): Extracting archive
  - Installing j0k3r/httplug-ssrf-plugin (v2.0.1): Extracting archive
  - Installing j0k3r/graby-site-config (1.0.128): Extracting archive
  - Installing ralouphie/getallheaders (3.0.3): Extracting archive
  - Installing guzzlehttp/psr7 (1.7.0): Extracting archive
  - Installing http-interop/http-factory-guzzle (1.0.0): Extracting archive
  - Installing fossar/htmlawed (1.2.8): Extracting archive
  - Installing j0k3r/graby (2.2.5): Extracting archive
  - Installing javibravo/simpleue (2.1.0): Extracting archive
  - Installing phpstan/phpdoc-parser (0.4.12): Extracting archive
  - Installing jms/metadata (2.5.0): Extracting archive
  - Installing doctrine/instantiator (1.4.0): Extracting archive
  - Installing jms/serializer (3.12.0): Extracting archive
  - Installing kphoen/rulerz (0.21.1): Extracting archive
  - Installing kphoen/rulerz-bridge (1.1.1): Extracting archive
  - Installing kphoen/rulerz-bundle (0.15.0): Extracting archive
  - Installing doctrine/orm (2.7.5): Extracting archive
  - Installing lexik/form-filter-bundle (v5.0.10): Extracting archive
  - Installing liip/theme-bundle (1.7.0): Extracting archive
  - Installing mgargano/simplehtmldom (1.5): Extracting archive
  - Installing mnapoli/piwik-twig-extension (3.0.0): Extracting archive
  - Installing michelf/php-markdown (1.9.0): Extracting archive
  - Installing nelmio/api-doc-bundle (2.13.4): Extracting archive
  - Installing nelmio/cors-bundle (1.5.6): Extracting archive
  - Installing paragonie/constant_time_encoding (v2.4.0): Extracting archive
  - Installing phpseclib/phpseclib (3.0.6): Extracting archive
  - Installing php-amqplib/php-amqplib (v2.12.3): Extracting archive
  - Installing php-amqplib/rabbitmq-bundle (v1.15.1): Extracting archive
  - Installing php-http/stopwatch-plugin (1.3.0): Extracting archive
  - Installing symfony/polyfill-php73 (v1.22.1): Extracting archive
  - Installing php-http/logger-plugin (1.2.1): Extracting archive
  - Installing php-http/guzzle5-adapter (2.0.0): Extracting archive
  - Installing symfony/http-client-contracts (v2.3.1): Extracting archive
  - Installing symfony/http-client (v5.2.4): Extracting archive
  - Installing php-http/httplug-bundle (1.19.0): Extracting archive
  - Installing pragmarx/random (v0.2.2): Extracting archive
  - Installing pragmarx/recovery (v0.1.0): Extracting archive
  - Installing predis/predis (v1.1.6): Extracting archive
  - Installing thecodingmachine/safe (v1.3.3): Extracting archive
  - Installing beberlei/assert (v3.3.0): Extracting archive
  - Installing spomky-labs/otphp (v10.0.1): Extracting archive
  - Installing lcobucci/jwt (3.4.5): Extracting archive
  - Installing scheb/two-factor-bundle (v4.18.4): Extracting archive
  - Installing symfony/mime (v5.2.5): Extracting archive
  - Installing sensiolabs/security-checker (v6.0.3): Extracting archive
  - Installing sensio/distribution-bundle (v5.0.25): Extracting archive
  - Installing sensio/framework-extra-bundle (v5.4.1): Extracting archive
  - Installing symfony/polyfill-uuid (v1.22.1): Extracting archive
  - Installing laminas/laminas-diactoros (2.4.1): Extracting archive
  - Installing jean85/pretty-package-versions (1.6.0): Extracting archive
  - Installing guzzlehttp/promises (1.4.1): Extracting archive
  - Installing sentry/sentry (2.5.2): Extracting archive
  - Installing sentry/sdk (2.2.0)
  - Installing sentry/sentry-symfony (3.5.3): Extracting archive
  - Installing gedmo/doctrine-extensions (v2.4.42): Extracting archive
  - Installing stof/doctrine-extensions-bundle (v1.3.0): Extracting archive
  - Installing symfony/monolog-bundle (v3.6.0): Extracting archive
  - Installing symfony/polyfill-iconv (v1.22.1): Extracting archive
  - Installing swiftmailer/swiftmailer (v6.2.7): Extracting archive
  - Installing symfony/swiftmailer-bundle (v3.3.1): Extracting archive
  - Installing tecnickcom/tcpdf (6.3.5): Extracting archive
  - Installing twig/extensions (v1.5.4): Extracting archive
  - Installing wallabag/php-mobi (1.1.0): Extracting archive
  - Installing phpzip/phpzip (2.0.8): Extracting archive
  - Installing grandt/phpresizegif (1.0.3): Extracting archive
  - Installing wallabag/phpepub (4.0.9): Extracting archive
  - Installing willdurand/hateoas (3.7.0): Extracting archive
  - Installing jms/serializer-bundle (3.8.0): Extracting archive
  - Installing willdurand/hateoas-bundle (2.1.0): Extracting archive
   0/148 [>---------------------------]   0%  10/148 [=>--------------------------]   6%  28/148 [=====>----------------------]  18%  45/148 [========>-------------------]  30%  63/148 [===========>----------------]  42%  73/148 [=============>--------------]  49%  83/148 [===============>------------]  56%  93/148 [=================>----------]  62% 111/148 [=====================>------]  75% 121/148 [======================>-----]  81% 138/148 [==========================>-]  93% 147/148 [===========================>]  99% 148/148 [============================] 100%Package doctrine/doctrine-cache-bundle is abandoned, you should avoid using it. No replacement was suggested.
Package doctrine/reflection is abandoned, you should avoid using it. Use roave/better-reflection instead.
Package guzzlehttp/ringphp is abandoned, you should avoid using it. No replacement was suggested.
Package guzzlehttp/streams is abandoned, you should avoid using it. No replacement was suggested.
Package liip/theme-bundle is abandoned, you should avoid using it. Use sylius/theme-bundle instead.
Package sensio/distribution-bundle is abandoned, you should avoid using it. No replacement was suggested.
Package sensiolabs/security-checker is abandoned, you should avoid using it. Use https://github.com/fabpot/local-php-security-checker instead.
Package twig/extensions is abandoned, you should avoid using it. No replacement was suggested.
Generating optimized autoload files
Class SimpleHtmlDom\\simple_html_dom_node located in ./vendor/mgargano/simplehtmldom/src/simple_html_dom.php does not comply with psr-0 autoloading standard. Skipping.
Class SimpleHtmlDom\\simple_html_dom located in ./vendor/mgargano/simplehtmldom/src/simple_html_dom.php does not comply with psr-0 autoloading standard. Skipping.
Class Sensio\\Bundle\\FrameworkExtraBundle\\Tests\\DependencyInjection\\AddExpressionLanguageProvidersPassTest located in ./vendor/sensio/framework-extra-bundle/Tests/DependencyInjection/Compiler/AddExpressionLanguageProvidersPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Sensio\\Bundle\\FrameworkExtraBundle\\Tests\\DependencyInjection\\AddParamConverterPassTest located in ./vendor/sensio/framework-extra-bundle/Tests/DependencyInjection/Compiler/AddParamConverterPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Sensio\\Bundle\\FrameworkExtraBundle\\Tests\equest\\ParamConverter\\ArgumentNameConverterTest located in ./vendor/sensio/framework-extra-bundle/Tests/Request/ArgumentNameConverterTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Nelmio\\ApiDocBundle\\Tests\\Extractor\\SensioFrameworkExtraHandlerTest located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Extractor/Handler/SensioFrameworkExtraHandlerTest.php does not comply with psr-0 autoloading standard. Skipping.
Class Nelmio\\ApiDocBundle\\Tests\\Extractor\\FosRestHandlerTest located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Extractor/Handler/FosRestHandlerTest.php does not comply with psr-0 autoloading standard. Skipping.
Class Nelmio\\ApiDocBundle\\Tests\\Functional\\AppKernel located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Fixtures/app/AppKernel.php does not comply with psr-0 autoloading standard. Skipping.
Class Liip\\ThemeBundle\\Tests\\DependencyInjection\\ThemeCompilerPassTest located in ./vendor/liip/theme-bundle/Tests/DependencyInjection/Compiler/ThemeCompilerPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Liip\\ThemeBundle\\Tests\\DependencyInjection\\AsseticTwigFormulaPassTest located in ./vendor/liip/theme-bundle/Tests/DependencyInjection/Compiler/AsseticTwigFormulaPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Liip\\ThemeBundle\\Tests\\EventListener\\UseCaseTest located in ./vendor/liip/theme-bundle/Tests/UseCaseTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Doctrine\\Bundle\\MigrationsBundle\\Tests\\DependencyInjection\\DoctrineCommandTest located in ./vendor/doctrine/doctrine-migrations-bundle/Tests/Command/DoctrineCommandTest.php does not comply with psr-4 autoloading standard. Skipping.
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
49 packages you are using are looking for funding.
Use the `composer fund` command to find out more!
> Incenteev\\ParameterHandler\\ScriptHandler::buildParameters
Creating the \"app/config/parameters.yml\" file
Some parameters are missing. Please provide them.
database_driver (pdo_mysql): pdo_pgsql
database_host (127.0.0.1): 127.0.0.1
database_port (null): 5432
database_name (wallabag): wallabag
database_user (root): wallabag
database_password (null): <secret>
database_path (null): 
database_table_prefix (wallabag_): wallabag_
database_socket (null): 
database_charset (utf8mb4): utf8
domain_name ('https://your-wallabag-url-instance.com'): https://wlbg.ourdomain.se
server_name ('Your wallabag instance'): wallabag
mailer_transport (smtp): smtp
mailer_user (null): 
mailer_password (null): 
mailer_host (127.0.0.1): 127.0.0.1
mailer_port (false): 
mailer_encryption (null): 
mailer_auth_mode (null): 
locale (en): en
secret (CHANGE_ME_TO_SOMETHING_SECRET_AND_RANDOM): <secret>
twofactor_auth (true): false
twofactor_sender (no-reply@wallabag.org): no-reply@wallabag.org
fosuser_registration (true): false
fosuser_confirmation (true): true
fos_oauth_server_access_token_lifetime (3600): 3600
fos_oauth_server_refresh_token_lifetime (1209600): 1209600
from_email (no-reply@wallabag.org): no-reply@wallabag.org
rss_limit (50): 50
rabbitmq_host (localhost): localhost
rabbitmq_port (5672): 5672
rabbitmq_user (guest): guest
rabbitmq_password (guest): guest
rabbitmq_prefetch_count (10): 10
redis_scheme (tcp): socket
redis_host (localhost): localhost
redis_port (6379): 
redis_path (null): /var/run/redis/redis-server.sock
redis_password (null): 
sentry_dsn (null): 
> Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::buildBootstrap
> Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::clearCache

 // Clearing the cache for the prod environment with debug                      
 // false                                                                       

                                                                                
 [OK] Cache for the "prod" environment (debug=false) was successfully cleared.  
                                                                                

> Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::installAssets

 Trying to install assets as relative symbolic links.

 --- ------------------------ ------------------ 
      Bundle                   Method / Error    
 --- ------------------------ ------------------ 
  ✔   NelmioApiDocBundle       relative symlink  
  ✔   CraueConfigBundle        relative symlink  
  ✔   BabDevPagerfantaBundle   relative symlink  
  ✔   FOSJsRoutingBundle       relative symlink  
 --- ------------------------ ------------------ 

                                                                                
 [OK] All assets were successfully installed.                                   
                                                                                

> Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::installRequirementsFile

wallabag installer
==================

Step 1 of 4: Checking system requirements.
------------------------------------------

 ------------------------ -------- ---------------- 
  Checked                  Status   Recommendation  
 ------------------------ -------- ---------------- 
  PDO Driver (pdo_pgsql)   OK!                      
  Database connection      OK!                      
  Database version         OK!                      
  curl_exec                OK!                      
  curl_multi_init          OK!                      
 ------------------------ -------- ---------------- 

                                                                                
 [OK] Success! Your system can run wallabag properly.                           
                                                                                

Step 2 of 4: Setting up database.
---------------------------------

 It appears that your database already exists. Would you like to reset it? (yes/no) [no]:

Creating schema...
Clearing the cache...

Database successfully setup.

Step 3 of 4: Administration setup.
----------------------------------

Would you like to create a new admin user (recommended)? (yes/no) [yes]:
> yes

Username [wallabag]:
> wallabag

Password [wallabag]:


Email [wallabag@wallabag.io]:
> webmaster@ourdomain.se

Administration successfully setup.

Step 4 of 4: Config setup.
--------------------------

Config successfully setup.

[OK] wallabag has been successfully installed.
[OK] You can now configure your web server, see https://doc.wallabag.org
```
