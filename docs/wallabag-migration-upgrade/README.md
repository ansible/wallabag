# How I migrated Wallabag from armant to taipei using this Ansible role

Detailing the steps taken migrating my live Wallabag instance on the bare-metal
host `armant` to the LXD container `taipei`.

On `armant` we have Wallabag v2.3.8 installed from source with
MySQL v5.7.37 as the database backend.

For this migration, we will change the backend to PostgreSQL and upgrade
to Wallabag v2.4.2.

Due to a weird SQL error during `make update` (see notes in `attempt-3-from_source.md`)
during the initial migration I will also switch from source to the precompiled
tarball Wallabag installation.

Out of an abundance of caution, and because our `postgres` role
easily supports it, and also because this role is designed for it,
we will use PostgreSQL v10 for Wallabag v2.3.8,
but upgrade to PostgreSQL v12 before upgrading to Wallabag to v2.4.2.

Before we begin, make sure the following is set in `defaults.yml`:
```
wlbg:
  version: 2.3.8
  source_install: no
  source_update: no
  symfony_env: "prod"
wallabag_database_pwd: "{{ lookup('community.general.passwordstore', 'hosts/armant/ubuntu-server/mysql/users/wallabag') }}"
database_postgresql: true
import_sql_database: true
import_method: "pgloader"
import_from:
  host: armant
  identityfile: "{{ ansible_env.HOME }}/.ssh/armant-taha"
  tunnel_port: 3306
import_ssh_config: |
  Host {{ import_from.host }}
  Hostname {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=hostname') }}
  Port {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=port') }}
  PreferredAuthentications publickey
  IdentityFile {{ import_from.identityfile }}
  IdentitiesOnly yes
  StrictHostKeyChecking no
  User {{ ansible_env.USER }}
```

Also make sure `psql_version: 10` is defined for the play (it might be possible
to use psql v12 from the get go - I haven't tested it).

Deleted and re-created the LXC container `taipei` using my `lxd-server` role:
```
taha@luxor:~
$ lxc stop taipei 
$ lxc delete taipei
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
$ ansible-playbook playbook-host.yml --ask-become-pass --ask-vault-pass --tags lxd-server
```

Ran this playbook to install Wallabag v2.3.8 from precompiled tarball on `taipei`
and to migrate our Wallabag database from `armant`:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei
```

OK, but note that the wallabag install task caused an error, but 
nonetheless completed without halting the play:
```
TASK [wallabag : php bin/console wallabag install] ********************************************************************************
changed: [taipei] => {"changed": true, "cmd": ["php", "bin/console", "wallabag:install", "--env=prod", "--no-interaction"], "delta": "0:00:02.214763", "end": "2022-02-09 21:09:18.449699", "msg": "", "rc": 0, "start": "2022-02-09 21:09:16.234936", "stderr":
  [Doctrine\\DBAL\\Exception\\TableNotFoundException]                             
  An exception occurred while executing 'SELECT NEXTVAL('\"user_id_seq\"')':     
  SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"user_id_seq\" does not exist
  LINE 1: SELECT NEXTVAL('\"user_id_seq\"')
                         ^                                                     
"stdout":
Wallabag installer
==================

Step 1 of 4: Checking system requirements.
------------------------------------------

 ------------------------ -------- ---------------- 
  Checked                  Status   Recommendation  
 ------------------------ -------- ---------------- 
  PDO Driver (pdo_pgsql)   OK!                      
  Database connection      OK!                      
  Database version         OK!                      
  curl_exec                OK!                      
  curl_multi_init          OK!                      
 ------------------------ -------- ---------------- 

 [OK] Success! Your system can run wallabag properly.                           

Step 2 of 4: Setting up database.
---------------------------------

 Clearing the cache...

 Database successfully setup.

Step 3 of 4: Administration setup.
----------------------------------
```
This error message can also be seen in `wallabag/var/logs/prod.log`. 
It does not seem to repeat over time, and does not seem to affect the behaviour
of Wallabag.

It seems the Wallabag instance behaves normally. Can also add new tags and articles
without any errors or warnings.

I think this "missing relations" error is resolved later on in this role when we
reset the Wallabag SQL sequences.

Alright, moving along, let's upgrade Wallabag from v2.3.8 to v2.4.2.
Note that since Wallabag v2.4.2 works better with PostgreSQL v12 (based on my own experience),
we should upgrade to PostgreSQL v12 first.

Reset `psql_version: 12`, and ran the `postgres` role:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags postgres
```

Seems to have worked nicely, even though the `postgres` role makes no effort to handle 
multiple versions (it seems the new version simply replaced the old one, but that's fine for me):
```
taha@taipei:~
$ psql -V
psql (PostgreSQL) 12.10 (Ubuntu 12.10-1.pgdg20.04+1)
```

Now let's upgrade Wallabag to the latest available release.
Make sure to reset these variables in `defaults.yml`:
```
wlbg:
  version: "latest"
  source_install: no
  source_update: no
  symfony_env: "prod"
import_sql_database: false # just to be safe
```
and run the playbook with `--tags "upgrade-wallabag"`:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags upgrade-wallabag
```

OK so far. Now we need to manually run the Wallabag upgrade SQL queries.

First, empty Wallabag's `var/cache/` again: 
```
taha@taipei:~/public/wallabag
$ sudo rm -rf var/cache/*
```

Now run the [PostgreSQL queries for v2.3 to v2.4 upgrade](https://doc.wallabag.org/en/admin/query-upgrade-23-24.html):
```
taha@taipei:~/public/wallabag
$ psql
psql (12.9 (Ubuntu 12.9-2.pgdg20.04+1), server 10.19 (Ubuntu 10.19-2.pgdg20.04+1))
Type "help" for help.

taha=> \c wallabag
psql (12.9 (Ubuntu 12.9-2.pgdg20.04+1), server 10.19 (Ubuntu 10.19-2.pgdg20.04+1))
You are now connected to database "wallabag" as user "taha".
wallabag=> ALTER TABLE wallabag_entry ADD archived_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;
ALTER TABLE
wallabag=> 
wallabag=> ALTER TABLE "wallabag_user" ADD googleAuthenticatorSecret VARCHAR(191) DEFAULT NULL;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_user" RENAME COLUMN twofactorauthentication TO emailTwoFactor;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_user" DROP trusted;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_user" ADD backupCodes TEXT DEFAULT NULL;
ALTER TABLE
wallabag=> 
wallabag=> ALTER TABLE wallabag_site_credential ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;
ALTER TABLE
wallabag=> 
wallabag=> ALTER TABLE wallabag_entry ADD hashed_url TEXT DEFAULT NULL;
ALTER TABLE
wallabag=> CREATE INDEX hashed_url_user_id ON wallabag_entry (user_id, hashed_url);
CREATE INDEX
wallabag=> 
wallabag=> ALTER TABLE "wallabag_config" RENAME COLUMN rss_token TO feed_token;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_config" RENAME COLUMN rss_limit TO feed_limit;
ALTER TABLE
wallabag=> 
wallabag=> ALTER TABLE "wallabag_oauth2_access_tokens" DROP CONSTRAINT FK_368A4209A76ED395;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_access_tokens" ADD CONSTRAINT FK_368A4209A76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_clients" DROP CONSTRAINT idx_user_oauth_client;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_clients" ADD CONSTRAINT FK_635D765EA76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_refresh_tokens" DROP CONSTRAINT FK_20C9FB24A76ED395;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_refresh_tokens" ADD CONSTRAINT FK_20C9FB24A76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_auth_codes" DROP CONSTRAINT FK_EE52E3FAA76ED395;
ALTER TABLE
wallabag=> ALTER TABLE "wallabag_oauth2_auth_codes" ADD CONSTRAINT FK_EE52E3FAA76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE
wallabag=> 
wallabag=> ALTER TABLE wallabag_entry ADD given_url TEXT DEFAULT NULL;
ALTER TABLE
wallabag=> ALTER TABLE wallabag_entry ADD hashed_given_url TEXT DEFAULT NULL;
ALTER TABLE
wallabag=> CREATE INDEX hashed_given_url_user_id ON wallabag_entry (user_id, hashed_given_url);
CREATE INDEX
wallabag=> 
wallabag=> UPDATE wallabag_config SET reading_speed = reading_speed*200;
UPDATE 3
wallabag=> 
wallabag=> ALTER TABLE "wallabag_entry" ALTER language TYPE VARCHAR(20);
ALTER TABLE
wallabag=> CREATE INDEX user_language ON "wallabag_entry" (language, user_id);
CREATE INDEX
wallabag=> CREATE INDEX user_archived ON "wallabag_entry" (user_id, is_archived, archived_at);
CREATE INDEX
wallabag=> CREATE INDEX user_created ON "wallabag_entry" (user_id, created_at);
CREATE INDEX
wallabag=> CREATE INDEX user_starred ON "wallabag_entry" (user_id, is_starred, starred_at);
CREATE INDEX
wallabag=> CREATE INDEX tag_label ON "wallabag_tag" (label);
CREATE INDEX
wallabag=> CREATE INDEX config_feed_token ON "wallabag_config" (feed_token);
CREATE INDEX
wallabag=> 
wallabag=> ALTER TABLE "wallabag_craue_config_setting" RENAME TO "wallabag_internal_setting";
ALTER TABLE
wallabag=> 
wallabag=> CREATE SEQUENCE ignore_origin_user_rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE
wallabag=> CREATE SEQUENCE ignore_origin_instance_rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE
wallabag=> CREATE TABLE wallabag_ignore_origin_user_rule (id SERIAL NOT NULL, config_id INT NOT NULL, rule VARCHAR(255) NOT NULL, PRIMARY KEY(id));
CREATE TABLE
wallabag=> CREATE INDEX idx_config ON wallabag_ignore_origin_user_rule (config_id);
CREATE INDEX
wallabag=> CREATE TABLE wallabag_ignore_origin_instance_rule (id SERIAL NOT NULL, rule VARCHAR(255) NOT NULL, PRIMARY KEY(id));
CREATE TABLE
wallabag=> ALTER TABLE wallabag_ignore_origin_user_rule ADD CONSTRAINT fk_config FOREIGN KEY (config_id) REFERENCES "wallabag_config" (id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE
wallabag=> 
wallabag=> UPDATE wallabag_internal_setting SET name = 'matomo_enabled' where name = 'piwik_enabled';
UPDATE 1
wallabag=> UPDATE wallabag_internal_setting SET name = 'matomo_host' where name = 'piwik_host';
UPDATE 1
wallabag=> UPDATE wallabag_internal_setting SET name = 'matomo_site_id' where name = 'piwik_site_id';
UPDATE 1
```

+ https://github.com/wallabag/wallabag/releases/tag/2.4.0  
Generate hash of URLs for all saved entries (improves API search):
```
taha@taipei:~/public/wallabag
$ sudo -u www-data php bin/console --env=prod wallabag:generate-hashed-urls
Generating hashed urls for "3" users
Processing user: idris
Generated hashed urls for user: idris
Processing user: wallabag
Generated hashed urls for user: wallabag
Processing user: taha
Generated hashed urls for user: taha
Finished generated hashed urls
```

Finally, clear the cache again:
```
taha@taipei:~/public/wallabag
$ sudo -u www-data php bin/console cache:clear --env=prod
```

OK!

Now let's do some testing:

+ add a new article in the webui? OK.
+ create a new tag in the webui? OK.
+ star an article in the webui? OK.
+ unstar and remove a tag? OK.
+ change an article title? OK.
+ mark an article as read? OK.
+ mark the same article as unread? OK.
+ setup new Wallabag Android app? Enter URL, username, password. 
  Test connection, OK, existing key and secret were fetched by Wallabag Android.
  Restarted the app and initiated "full update". OK.
+ from the app, starred an article. OK.
+ from the app, added a new tag. OK.
+ seems to work OK from a newly configured Wallabag app.
+ what about from a pre-existing Wallabag app (recall, we have migrated an existing Wallabag instance)?
  seems to work seamlessly. fantastic.
+ we also have pre-existing API access from TinyTinyRSS. Can we send an article from TTRSS
  to Wallabag?
  That caused an error:
```
Key (id)=(1833) already exists. at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/AbstractPostgreSQLDriver.php:57, Doctrine\\DBAL\\Driver\\PDOException(code: 23505): SQLSTATE[23505]: Unique violation: 7 ERROR: duplicate key value violates unique constraint idx_16419_primary
```
  This required clearing the Wallabag plugin data in TinyTinyRSS, disabling it, 
  wiping the Wallabag cache directory and clearing its cache, then re-enabling
  the Wallabag plugin in TTRSS and resupplying our credentials (for good measure I
  deleted the original API client and created a new one in Wallabag, not sure that
  was necessary, but I think so). And after that, adding articles to Wallabag from
  our TTRSS instance works again. OK.

With that, I think we look good overall. Our live URL points to our migrated
Wallabag instance which appears to work normally.
Will give it a week or so to see if we can discover any more lurking bugs.
