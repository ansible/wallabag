# How I migrated Wallabag from armant to taipei using this Ansible role

> My previous two attempts were actually migrating Wallabag installed from
> source to Wallabag installed from pre-compiled binary.
> Turns out I was mistaken - my Wallabag instance on `armant` was, and 
> always was, installed from source (git repo).
> This does definitely prove, though, that migrating from Wallabag installed
> from precompiled binary to source install works.


Detailing the steps taken migrating my live Wallabag instance on the bare-metal
host `armant` to the LXD container `taipei`.

On `armant` we have Wallabag v2.3.8 installed from source with
MySQL v5.7.37 as the database backend.

For this migration, we will change the backend to PostgreSQL and upgrade
to Wallabag v2.4.2.

Testing has demonstrated that we need to use PostgreSQL v10 initially,
since Wallabag v2.3.8 expects it, but we will upgrade to PostgreSQL v12 
at the same time as upgrading Wallabag to v2.4.2.

At the outset, set the following in `defaults.yml`:
```
wlbg:
  version: 2.3.8
  source_install: yes
  source_update: no
  symfony_env: "prod"
wallabag_database_pwd: "{{ lookup('community.general.passwordstore', 'hosts/armant/ubuntu-server/mysql/users/wallabag') }}"
database_postgresql: true
import_sql_database: true
import_method: "pgloader"
import_from:
  host: armant
  identityfile: "{{ ansible_env.HOME }}/.ssh/armant-taha"
  tunnel_port: 3306
import_ssh_config: |
  Host {{ import_from.host }}
  Hostname {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=hostname') }}
  Port {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=port') }}
  PreferredAuthentications publickey
  IdentityFile {{ import_from.identityfile }}
  IdentitiesOnly yes
  StrictHostKeyChecking no
  User {{ ansible_env.USER }}
```

Also set `psql_version: 10` in `group_vars`.

Deleted and re-created the LXC container `taipei`:
```
taha@luxor:~
$ lxc stop taipei 
$ lxc delete taipei
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
$ ansible-playbook playbook-host.yml --ask-become-pass --ask-vault-pass --tags lxd-server
```

Ran this playbook to install Wallabag v2.3.8 from source on `taipei`
and to migrate our Wallabag database from `armant`:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei
```

OK, but note that the wallabag admin user creation task caused an error, but 
nonetheless completed without halting the play:
```
TASK [wallabag : Create wallabag admin user with default password] ****************************************************************
changed: [taipei] => {"changed": true, "cmd": ["php", "bin/console", "wallabag:install", "--env=prod", "--no-interaction"], "delta": "0:00:02.214763", "end": "2022-02-09 21:09:18.449699", "msg": "", "rc": 0, "start": "2022-02-09 21:09:16.234936", "stderr":
  [Doctrine\\DBAL\\Exception\\TableNotFoundException]                             
  An exception occurred while executing 'SELECT NEXTVAL('\"user_id_seq\"')':     
  SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"user_id_seq\" does not exist
  LINE 1: SELECT NEXTVAL('\"user_id_seq\"')
                         ^                                                     
"stdout":
Wallabag installer
==================

Step 1 of 4: Checking system requirements.
------------------------------------------

 ------------------------ -------- ---------------- 
  Checked                  Status   Recommendation  
 ------------------------ -------- ---------------- 
  PDO Driver (pdo_pgsql)   OK!                      
  Database connection      OK!                      
  Database version         OK!                      
  curl_exec                OK!                      
  curl_multi_init          OK!                      
 ------------------------ -------- ---------------- 

 [OK] Success! Your system can run wallabag properly.                           

Step 2 of 4: Setting up database.
---------------------------------

 Clearing the cache...

 Database successfully setup.

Step 3 of 4: Administration setup.
----------------------------------
```
This error message can also be seen in `wallabag/var/logs/prod.log`. 
It does not seem to repeat over time, and does not seem to affect the behaviour
of Wallabag.

It seems the Wallabag instance behaves normally. Can also add new tags and articles
without any errors or warnings.

Alright, moving along, let's upgrade Wallabag from v2.3.8 to v2.4.2.
Note that since Wallabag v2.4.2 works better with PostgreSQL v12 (based on my own experience),
we should upgrade to PostgreSQL v12 first.

Reset `psql_version: 12`, and ran the `postgres` role:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags postgres
```

Seems to have worked nicely, even though the `postgres` role makes no effort to handle 
multiple versions (it seems the new version simply replaced the old one):
```
taha@taipei:~
$ psql -V
psql (PostgreSQL) 12.10 (Ubuntu 12.10-1.pgdg20.04+1)
```

Now let's upgrade Wallabag to the latest available release.
Reset these variables in `defaults.yml`:
```
wlbg:
  version: "latest"
  source_install: yes
  source_update: yes
  symfony_env: "prod"
import_sql_database: false # just to be safe
```
and run the playbook with `--tags "upgrade-wallabag"`:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags upgrade-wallabag
```



Almost...
The `make update` task fails at `php bin/console doctrine:migrations:migrate --no-interaction --env=prod`:
```
> Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::installRequirementsFile
                                                             
                    Application Migrations                    

Migrating up to 20200428072628 from 0
  ++ migrating 20160401000000

Migration 20160401000000 failed during Execution. 
Error In AbstractPostgreSQLDriver.php line 89:

  An exception occurred while executing 'SELECT quote_ident(r.conname) as conname, pg_catalog.pg_get_constraintdef(r.oid, true) as condef
  FROM pg_catalog.pg_constraint r WHERE r.conrelid = (
    SELECT c.oid FROM pg_catalog.pg_class c, pg_catalog.pg_namespace n 
    WHERE n.nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') 
    AND c.relname = 'migration_versions' AND n.nspname = ANY(current_schemas(false)) 
    AND n.oid = c.relnamespace)
  AND r.contype = 'f'':
  SQLSTATE[21000]: Cardinality violation: 7 
  ERROR: more than one row returned by a subquery used as an expression

In PDOConnection.php line 91:
  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression  
                                                                                                                    
In PDOConnection.php line 86:
  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression  

make: *** [GNUmakefile:24: update] Error 1
```

I tried to apply the [Wallabag upgrade SQL queries](https://doc.wallabag.org/en/admin/query-upgrade-23-24.html)
before running `make update`, but it made no difference, exactly the same error as above.

Could this be because we are using a too modern PostgreSQL version?
I have a really hard time finding documentation on which version is recommended 
for Wallabag v2.4.2.

+ https://github.com/doctrine/dbal/issues/2868
+ https://github.com/wallabag/wallabag/issues/3479
+ [even Wallabag 2.3.8 should support PostgreSQL 12](https://github.com/wallabag/docker/issues/168#issuecomment-654021001)
+ [here's another user mentioning in passing that they're using PostgreSQL 12](https://github.com/wallabag/wallabag/issues/4473), and no one raised any objections...

I don't think downgrading to PostgreSQL v11 or even v10 would make any difference
(not tested, though).

+ https://github.com/wallabag/wallabag/issues/4987

The weird thing is that the very same migration and upgrade worked fine when I
used Wallabag's precompiled archive on the new host.
Why should I even bother with installing from source, if it is just more fragile,
for no obvious benefit?
Well, this kills the cat.



**A small remark regarding file ownership and database host and port in `parameters.yml`.**
Leaving `database_host` and `database_port` empty (a change I recently implemented
for PostgreSQL 12 thinking that peer auth made them redundant) makes all Wallabag's
`php bin/console` fail as long as the files are owned by a user other than the user
that owns the PostgreSQL `wallabag` database (which is our regular user).

To summarise, with Wallabag's files owned by `www-data`, `database_host: localhost`
and `database_port: 5432` had to be set to avoid the fatal error
`Peer authentication failed for user "taha"`.

With Wallabag's files owned by `taha`, the login page is rendered, but all attempts
to login fail silently and the Apache error log shows `Permission denied` errors
(clearly the apache user cannot create files/folders in the Wallabag tree).

So, `www-data` needs to own the Wallabag tree, and the database host and port
fields in `parameters.yml` need to be set, even for PostgreSQL that uses peer auth
by default.
