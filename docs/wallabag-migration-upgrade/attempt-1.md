# Detailing the steps taken migrating my live Wallabag instance

Detailing the steps taken migrating my live Wallabag instance on bare-metal host
`armant` to LXD container `taipei`.

On `armant`: Wallabag v2.3.8 installed from precompiled tarball using MySQL backend.

I intend to change backend to PostgreSQL, but will stick with precompiled tarball
since my earlier attempts to switch proved futile.

In `defaults.yml`, set:
```
wlbg_install_from_source: no
wlbg_version: 2.3.8
wallabag_database_pwd: "{{ lookup('community.general.passwordstore', 'hosts/armant/ubuntu-server/mysql/users/wallabag') }}"
database_postgresql: true
import_sql_database: true
import_method: "pgloader"
import_from:
  host: armant
  identityfile: "{{ ansible_env.HOME }}/.ssh/armant-taha"
  tunnel_port: 3306
import_ssh_config: |
  Host {{ import_from.host }}
  Hostname {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=hostname') }}
  Port {{ lookup('community.general.passwordstore', 'hosts/armant/hostinfo subkey=port') }}
  PreferredAuthentications publickey
  IdentityFile {{ import_from.identityfile }}
  IdentitiesOnly yes
  StrictHostKeyChecking no
  User {{ ansible_env.USER }}
```

Deleted and re-created the LXC container `taipei`.

Ran the containers playbook to install Wallabag v2.3.8 pre-compiled on `taipei`
and to migrate database from `armant` (note: with `psql_version: 10`):
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei
```

NOTE! The wallabag admin user creation task caused an error, but nonetheless completed without halting the play:
```
TASK [wallabag : Create wallabag admin user with default password] ****************************************************************
changed: [taipei] => {"changed": true, "cmd": ["php", "bin/console", "wallabag:install", "--env=prod", "--no-interaction"], "delta": "0:00:02.214763", "end": "2022-02-09 21:09:18.449699", "msg": "", "rc": 0, "start": "2022-02-09 21:09:16.234936", "stderr":
  [Doctrine\\DBAL\\Exception\\TableNotFoundException]                             
  An exception occurred while executing 'SELECT NEXTVAL('\"user_id_seq\"')':     
  SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"user_id_seq\" does not exist
  LINE 1: SELECT NEXTVAL('\"user_id_seq\"')
                         ^                                                     
"stdout":
Wallabag installer
==================

Step 1 of 4: Checking system requirements.
------------------------------------------

 ------------------------ -------- ---------------- 
  Checked                  Status   Recommendation  
 ------------------------ -------- ---------------- 
  PDO Driver (pdo_pgsql)   OK!                      
  Database connection      OK!                      
  Database version         OK!                      
  curl_exec                OK!                      
  curl_multi_init          OK!                      
 ------------------------ -------- ---------------- 

 [OK] Success! Your system can run wallabag properly.                           

Step 2 of 4: Setting up database.
---------------------------------

 Clearing the cache...

 Database successfully setup.

Step 3 of 4: Administration setup.
----------------------------------
```
This error can also be seen in `wallabag/var/logs/prod.log`. It does not seem to repeat over time, though.
Also, why did this task simply report "changed" and not "fail" and cause the playbook to halt?

Well, seems the Wallabag instance behaves normally. Can also add new tags and articles
without any errors or warnings.

Well then, let's attempt to upgrade Wallabag from 2.3.8 to 2.4.2 and then PostgreSQL to v12.
No need to worry about backups at this point.
To upgrade Wallabag, we can run the playbook with `--tags "upgrade-wallabag"`, after 
resetting `wlbg_version: 2.4.2` and `import_sql_database: false`:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags upgrade-wallabag
```

This almost finished without errors, but the handler **Clear the wallabag symfony cache** threw the error:
```
fatal: [taipei]: FAILED! => {"changed": true, "cmd": ["php", "bin/console", "cache:clear", "--env=prod"], "delta": "0:00:00.810843", "end": "2022-02-09 22:36:56.628301", "msg": "non-zero return code", "rc": 1, "start": "2022-02-09 22:36:55.817458", "stderr":
In AbstractPostgreSQLDriver.php line 75:
  An exception occurred in driver: SQLSTATE[08006] [7] FATAL: Peer authentication failed for user "taha"
```
I assume this is because we are still on PostgreSQL v10, whereas the Wallabag parameters file
is now configured for Wallabag 2.4.2, which sort of assumes the system is using PostgreSQL v12.

So, let's upgrade the PostgreSQL installation at this point.
Reset `psql_version: 12`, and ran the postgres role:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags postgres
```

Seems to have worked nicely, even though the postgres role makes no effort to handle 
multiple versions (it seems the new version simply replaced the old one):
```
taha@taipei:~
$ psql -V
psql (PostgreSQL) 12.9 (Ubuntu 12.9-2.pgdg20.04+1)
```

With that, let's retry the wallabag upgrade:
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags upgrade-wallabag
```


With `psql_version: 12` I got error
