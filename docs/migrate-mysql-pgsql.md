# Migrating MySQL database to PostgreSQL

https://codeberg.org/ansible/wallabag/issues/1


## pgloader

This almost worked, except for [a critical bug that don't allow anyone to add articles](https://codeberg.org/ansible/wallabag/issues/1).

+ https://pgloader.readthedocs.io/en/latest/pgloader.html#pgloader-commands-syntax 
+ https://www.digitalocean.com/community/tutorials/how-to-migrate-mysql-database-to-postgres-using-pgloader
+ https://docs.gitlab.com/ee/update/mysql_to_postgresql.html


```
2021-06-14T02:21:54.010000Z LOG pgloader version "3.6.2"
2021-06-14T02:21:54.032000Z LOG Migrating from #<MYSQL-CONNECTION mysql://wallabag@localhost:3306/wallabag {1005572F83}>
2021-06-14T02:21:54.032000Z LOG Migrating into #<PGSQL-CONNECTION pgsql://taha@UNIX:5432/wallabag {10056C93E3}>
2021-06-14T02:21:56.141000Z LOG report summary reset
                             table name     errors       rows      bytes      total time
---------------------------------------  ---------  ---------  ---------  --------------
                        fetch meta data          0         70                     0.080s
                         Create Schemas          0          0                     0.002s
                       Create SQL Types          0          0                     0.004s
                          Create tables          0         28                     0.186s
                         Set Table OIDs          0         14                     0.021s
---------------------------------------  ---------  ---------  ---------  --------------
            wallabag.migration_versions          0         36     0.5 kB          0.025s
           wallabag.wallabag_annotation          0          1     0.4 kB          0.028s
               wallabag.wallabag_config          0          2     0.1 kB          0.053s
 wallabag.wallabag_craue_config_setting          0         33     0.9 kB          0.022s
            wallabag.wallabag_entry_tag          0        294     1.8 kB          0.096s
                wallabag.wallabag_entry          0       1450    33.0 MB          1.477s
    wallabag.wallabag_oauth2_auth_codes          0          0                     0.035s
wallabag.wallabag_oauth2_refresh_tokens          0      11196     1.2 MB          0.110s
                  wallabag.wallabag_tag          0         31     0.7 kB          0.031s
                 wallabag.wallabag_user          0          2     0.6 kB          0.036s
 wallabag.wallabag_oauth2_access_tokens          0      11522     1.2 MB          0.238s
       wallabag.wallabag_oauth2_clients          0          3     0.7 kB          0.055s
      wallabag.wallabag_site_credential          0          0                     0.056s
         wallabag.wallabag_tagging_rule          0          0                     0.049s
---------------------------------------  ---------  ---------  ---------  --------------
                COPY Threads Completion          0          4                     1.553s
                         Create Indexes          0         41                     0.474s
                 Index Build Completion          0         41                     0.048s
                        Reset Sequences          0         11                     0.021s
                           Primary Keys          0         14                     0.006s
                    Create Foreign Keys          0         15                     0.024s
                        Create Triggers          0          0                     0.000s
                        Set Search Path          0          1                     0.000s
                       Install Comments          0          6                     0.001s
---------------------------------------  ---------  ---------  ---------  --------------
                      Total import time          ✓      24570    35.4 MB          2.127s
```

Most everything works, except for **adding new articles**.
Let's take a look at the migrated PGSQL database.

```
taha@taipei:~/public/wallabag
((2.3.8)) $ psql -d wallabag
psql (12.7 (Ubuntu 12.7-0ubuntu0.20.04.1))
Type "help" for help.

wallabag=> select column_name, data_type from information_schema.columns where table_name='wallabag_user';
       column_name       |        data_type         
-------------------------+--------------------------
 id                      | bigint
 username                | character varying
 username_canonical      | character varying
 email                   | character varying
 email_canonical         | character varying
 enabled                 | boolean
 salt                    | character varying
 password                | character varying
 last_login              | timestamp with time zone
 confirmation_token      | character varying
 password_requested_at   | timestamp with time zone
 roles                   | text
 name                    | text
 created_at              | timestamp with time zone
 updated_at              | timestamp with time zone
 authcode                | bigint
 twofactorauthentication | boolean
 trusted                 | text
(18 rows)
```

Ok, note how the `pgloader` migration tool has lower-cased the table names that contained capital letters in MySQL.
This is important, since PostgreSQL is case-sensitive, whereas MySQL is not (as far as I understand).

When adding a new article (using the plus button), this happens:
```
2021-06-13 22:40:09] request.INFO: Matched route "new_entry". {"route":"new_entry","route_parameters":{"_controller":"Wallabag\\CoreBundle\\Controller\\EntryController::addEntryFormAction","_route":"new_entry"},"request_uri":"http://wlbg.chepec.se/new-entry","method":"POST"} []
[2021-06-13 22:40:09] security.DEBUG: Read existing security token from the session. {"key":"_security_secured_area"} []
[2021-06-13 22:40:09] security.DEBUG: User was reloaded from a user provider. {"username":"taha","provider":"Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider"} []
[2021-06-13 22:40:10] app.DEBUG: Restricted access config enabled? {"enabled":0} []
[2021-06-13 22:40:10] graby.INFO: Graby is ready to fetch [] []
[2021-06-13 22:40:10] graby.INFO: . looking for site config for phenomenalworld.org in primary folder {"host":"phenomenalworld.org"} []
[2021-06-13 22:40:10] graby.INFO: Appending site config settings from global.txt [] []
[2021-06-13 22:40:10] graby.INFO: . looking for site config for global in primary folder {"host":"global"} []
[2021-06-13 22:40:10] graby.INFO: ... found site config global.txt {"host":"global.txt"} []
[2021-06-13 22:40:10] graby.INFO: Cached site config with key: phenomenalworld.org {"key":"phenomenalworld.org"} []
[2021-06-13 22:40:10] graby.INFO: . looking for site config for global in primary folder {"host":"global"} []
[2021-06-13 22:40:10] graby.INFO: ... found site config global.txt {"host":"global.txt"} []
[2021-06-13 22:40:10] graby.INFO: Appending site config settings from global.txt [] []
[2021-06-13 22:40:10] graby.INFO: Cached site config with key: global {"key":"global"} []
[2021-06-13 22:40:10] graby.INFO: Cached site config with key: phenomenalworld.org.merged {"key":"phenomenalworld.org.merged"} []
[2021-06-13 22:40:10] graby.INFO: Fetching url: https://phenomenalworld.org/analysis/ever-given {"url":"https://phenomenalworld.org/analysis/ever-given"} []
[2021-06-13 22:40:10] graby.INFO: Trying using method "get" on url "https://phenomenalworld.org/analysis/ever-given" {"method":"get","url":"https://phenomenalworld.org/analysis/ever-given"} []
[2021-06-13 22:40:10] graby.INFO: Use default user-agent "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.92 Safari/535.2" for url "https://phenomenalworld.org/analysis/ever-given" {"user-agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.92 Safari/535.2","url":"https://phenomenalworld.org/analysis/ever-given"} []
[2021-06-13 22:40:10] graby.INFO: Use default referer "http://www.google.co.uk/url?sa=t&source=web&cd=1" for url "https://phenomenalworld.org/analysis/ever-given" {"referer":"http://www.google.co.uk/url?sa=t&source=web&cd=1","url":"https://phenomenalworld.org/analysis/ever-given"} []
[2021-06-13 22:40:10] graby.INFO: Data fetched: [array] {"data":{"effective_url":"https://phenomenalworld.org/analysis/ever-given","body":"(only length for debug): 42192","headers":"text/html; charset=UTF-8","all_headers":{"date":"Mon, 14 Jun 2021 03:40:10 GMT","server":"Apache/2.4.29 (Ubuntu)","vary":"Accept-Encoding","transfer-encoding":"chunked","content-type":"text/html; charset=UTF-8"},"status":200}} []
[2021-06-13 22:40:10] graby.INFO: Treating as UTF-8 {"encoding":"utf-8"} []
[2021-06-13 22:40:10] graby.DEBUG: Fetched HTML {"html":"<...HTML source of webpage, redacted...>"} []
[2021-06-13 22:40:10] graby.DEBUG: HTML after regex empty nodes stripping {"html":"<...HTML source of webpage, redacted...>"} []
[2021-06-13 22:40:10] graby.INFO: Opengraph data: [array] {"ogData":{"og_title":"Crisis Canal | Mona Ali","og_description":"Trade, bond markets, Suez, and the Ever Given.","og_url":"https://phenomenalworld.org/analysis/ever-given","og_image":"https://phenomenalworld.org/media/pages/analysis/ever-given/467ecbe94b-1623164388/ever-given-phenomenal-world-jain-family-institute-mona-ali-trade.jpeg","og_type":"website","og_site_name":"Phenomenal World"}} []
[2021-06-13 22:40:10] graby.INFO: Looking for site config files to see if single page link exists [] []
[2021-06-13 22:40:10] graby.INFO: Returning cached and merged site config for phenomenalworld.org {"host":"phenomenalworld.org"} []
[2021-06-13 22:40:10] graby.INFO: No "single_page_link" config found [] []
[2021-06-13 22:40:10] graby.INFO: Attempting to extract content [] []
[2021-06-13 22:40:10] graby.INFO: Returning cached and merged site config for phenomenalworld.org {"host":"phenomenalworld.org"} []
[2021-06-13 22:40:10] graby.DEBUG: Actual site config {"siteConfig":"[object] (Graby\\SiteConfig\\SiteConfig: {\"title\":[\"//meta[@property=\\\"og:title\\\"]/@content\"],\"body\":[],\"author\":[],\"date\":[\"//meta[@property=\\\"article:published_time\\\"]/@content\"],\"strip\":[],\"strip_attr\":[],\"src_lazy_load_attr\":null,\"strip_id_or_class\":[],\"strip_image_src\":[\"doubleclick.net\"],\"native_ad_clue\":[],\"http_header\":[],\"tidy\":null,\"autodetect_on_failure\":null,\"prune\":null,\"test_url\":[],\"if_page_contains\":[],\"single_page_link\":[],\"next_page_link\":[],\"parser\":null,\"find_string\":[\"<amp-img\",\"</amp-img>\",\"<amp-img\",\"</amp-img>\"],\"replace_string\":[\"<img\",\"<!-- nothing -->\",\"<img\",\"<!-- nothing -->\"],\"cache_key\":null,\"requires_login\":false,\"not_logged_in_xpath\":false,\"login_uri\":false,\"login_username_field\":false,\"login_password_field\":false,\"login_extra_fields\":[],\"skip_json_ld\":false})"} []
[2021-06-13 22:40:10] graby.INFO: Strings replaced: 0 (find_string and/or replace_string) {"count":0} []
[2021-06-13 22:40:10] graby.DEBUG: HTML after site config strings replacements {"html":"<...HTML source of webpage, redacted...>"} []
[2021-06-13 22:40:10] graby.INFO: Attempting to parse HTML with libxml {"parser":"libxml"} []
[2021-06-13 22:40:10] graby.INFO: Body size after Readability: 32242 {"length":32242} []
[2021-06-13 22:40:10] graby.DEBUG: Body after Readability {"dom_saveXML":"<...XML code of webpage body, redacted...>"} []
[2021-06-13 22:40:10] graby.INFO: Trying //meta[@property="og:title"]/@content for title {"pattern":"//meta[@property=\"og:title\"]/@content"} []
[2021-06-13 22:40:10] graby.INFO: title matched: Crisis Canal | Mona Ali {"title":"Crisis Canal | Mona Ali"} []
[2021-06-13 22:40:10] graby.INFO: ...XPath match: {pattern} ["pattern","//meta[@property=\"og:title\"]/@content"] []
[2021-06-13 22:40:10] graby.INFO: Trying //meta[@property="article:published_time"]/@content for date {"pattern":"//meta[@property=\"article:published_time\"]/@content"} []
[2021-06-13 22:40:10] graby.INFO: Trying //html[@lang]/@lang for language {"pattern":"//html[@lang]/@lang"} []
[2021-06-13 22:40:10] graby.INFO: Language matched:  {"language":""} []
[2021-06-13 22:40:10] graby.DEBUG: DOM after site config stripping {"dom_saveXML":"<...XML code of webpage body, redacted...>"} []
[2021-06-13 22:40:10] graby.INFO: Using Readability [] []
[2021-06-13 22:40:10] graby.INFO: Date is bad (strtotime failed):  {"date":null} []
[2021-06-13 22:40:10] graby.INFO: Detecting body [] []
[2021-06-13 22:40:10] graby.INFO: Pruning content [] []
[2021-06-13 22:40:10] graby.INFO: Success ? 1 {"is_success":true} []
[2021-06-13 22:40:10] graby.DEBUG: Body after cleanupHtml, before cleanupXss {"html":"<...HTML body after cleanup, redacted...>"} []
[2021-06-13 22:40:10] graby.INFO: Filtering HTML to remove XSS [] []
[2021-06-13 22:40:10] graby.INFO: Returning data (most interesting ones): [array] {"data":{"html":"(only length for debug): 20683","status":200,"title":"Crisis Canal | Mona Ali","language":"","date":null,"authors":[],"url":"https://phenomenalworld.org/analysis/ever-given","content_type":"text/html","open_graph":{"og_title":"Crisis Canal | Mona Ali","og_description":"Trade, bond markets, Suez, and the Ever Given.","og_url":"https://phenomenalworld.org/analysis/ever-given","og_image":"https://phenomenalworld.org/media/pages/analysis/ever-given/467ecbe94b-1623164388/ever-given-phenomenal-world-jain-family-institute-mona-ali-trade.jpeg","og_type":"website","og_site_name":"Phenomenal World"},"native_ad":false,"all_headers":{"date":"Mon, 14 Jun 2021 03:40:10 GMT","server":"Apache/2.4.29 (Ubuntu)","vary":"Accept-Encoding","transfer-encoding":"chunked","content-type":"text/html; charset=UTF-8"}}} []

[2021-06-13 22:40:10] request.CRITICAL: Uncaught PHP Exception Doctrine\DBAL\Exception\TableNotFoundException: "An exception occurred while executing 'SELECT NEXTVAL('"entry_id_seq"')':  SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation "entry_id_seq" does not exist LINE 1: SELECT NEXTVAL('"entry_id_seq"')                        
^" at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/AbstractPostgreSQLDriver.php line 75 {"exception":"[object] (Doctrine\\DBAL\\Exception\\TableNotFoundException(code: 0): An exception occurred while executing 'SELECT NEXTVAL('\"entry_id_seq\"')':

SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"entry_id_seq\" does not exist
LINE 1: SELECT NEXTVAL('\"entry_id_seq\"')
   ^ at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/AbstractPostgreSQLDriver.php:75, Doctrine\\DBAL\\Driver\\PDOException(code: 42P01): SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"entry_id_seq\" does not exist
LINE 1: SELECT NEXTVAL('\"entry_id_seq\"')
   ^ at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:106, PDOException(code: 42P01): SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation \"entry_id_seq\" does not exist
LINE 1: SELECT NEXTVAL('\"entry_id_seq\"')
   ^ at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:104)"} []
[2021-06-13 22:40:10] security.DEBUG: Stored the security token in the session. {"key":"_security_secured_area"} []
```

Perhaps I should make sure this is not due to something in my existing database.  
**That is, setup a new v2.3.8 Wallabag using MySQL, populate it with some articles, and then
perform the migration again.**




## Other tools to migrate from mysql to pgsql

+ https://github.com/AnatolyUss/FromMySqlToPostgreSql (last commit 2020-04-08, 275 stars, 68 forks)
+ https://github.com/philipsoutham/py-mysql2pgsql (last commit 2017-10-20, 419 stars, 168 forks)
+ https://github.com/lanyrd/mysql-postgresql-converter (last commit 2016-03-07, 1.2k stars, 428 forks)
+ https://github.com/AnatolyUss/nmig (last commit 2020-10-28, 250 stars, 43 forks)

Try philipsoutham's tool first, then perhaps NMIG.

### philipsoutham

I couldn't install it. Project is old, probably outdated pip dependencies.


### NMIG

Database migration appears to have worked.

Can't login to Wallabag afterwards, though:
```
[2021-06-13 15:04:36] request.INFO: Matched route "fos_user_security_check". {"route":"fos_user_security_check","route_parameters":{"_controller":"Wallabag\\UserBundle\\Controller\\SecurityController::checkAction","_route":"fos_user_security_check"},"request_uri":"http://wlbg.chepec.se/login_check","method":"POST"} []
[2021-06-13 15:04:36] app.ERROR: Authentication failure for user "taha", from IP "10.252.116.1", with UA: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36". [] []
[2021-06-13 15:04:36] security.INFO: Authentication request failed. {"exception":"[object] (Symfony\\Component\\Security\\Core\\Exception\\AuthenticationServiceException(code: 0): An exception occurred while executing 'SELECT t0.username AS username_1, t0.username_canonical AS username_canonical_2, t0.email AS email_3, t0.email_canonical AS email_canonical_4, t0.enabled AS enabled_5, t0.salt AS salt_6, t0.password AS password_7, t0.last_login AS last_login_8, t0.confirmation_token AS confirmation_token_9, t0.password_requested_at AS password_requested_at_10, t0.roles AS roles_11, t0.id AS id_12, t0.name AS name_13, t0.created_at AS created_at_14, t0.updated_at AS updated_at_15, t0.authCode AS authcode_16, t0.twoFactorAuthentication AS twofactorauthentication_17, t0.trusted AS trusted_18, t19.id AS id_20, t19.theme AS theme_21, t19.items_per_page AS items_per_page_22, t19.language AS language_23, t19.rss_token AS rss_token_24, t19.rss_limit AS rss_limit_25, t19.reading_speed AS reading_speed_26, t19.pocket_consumer_key AS pocket_consumer_key_27, t19.action_mark_as_read AS action_mark_as_read_28, t19.list_mode AS list_mode_29, t19.user_id AS user_id_30 FROM \"wallabag_user\" t0 LEFT JOIN \"wallabag_config\" t19 ON t19.user_id = t0.id WHERE t0.username_canonical = ? LIMIT 1' with params [\"taha\"]:
SQLSTATE[42703]: Undefined column: 7 ERROR:  column t0.authcode does not exist
LINE 1: ...AS created_at_14, t0.updated_at AS updated_at_15, t0.authCod...
HINT:  Perhaps you meant to reference the column \"t0.authCode\". at /home/taha/public/wallabag/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Authentication/Provider/DaoAuthenticationProvider.php:92, Doctrine\\DBAL\\Exception\\InvalidFieldNameException(code: 0): An exception occurred while executing 'SELECT t0.username AS username_1, t0.username_canonical AS username_canonical_2, t0.email AS email_3, t0.email_canonical AS email_canonical_4, t0.enabled AS enabled_5, t0.salt AS salt_6, t0.password AS password_7, t0.last_login AS last_login_8, t0.confirmation_token AS confirmation_token_9, t0.password_requested_at AS password_requested_at_10, t0.roles AS roles_11, t0.id AS id_12, t0.name AS name_13, t0.created_at AS created_at_14, t0.updated_at AS updated_at_15, t0.authCode AS authcode_16, t0.twoFactorAuthentication AS twofactorauthentication_17, t0.trusted AS trusted_18, t19.id AS id_20, t19.theme AS theme_21, t19.items_per_page AS items_per_page_22, t19.language AS language_23, t19.rss_token AS rss_token_24, t19.rss_limit AS rss_limit_25, t19.reading_speed AS reading_speed_26, t19.pocket_consumer_key AS pocket_consumer_key_27, t19.action_mark_as_read AS action_mark_as_read_28, t19.list_mode AS list_mode_29, t19.user_id AS user_id_30 FROM \"wallabag_user\" t0 LEFT JOIN \"wallabag_config\" t19 ON t19.user_id = t0.id WHERE t0.username_canonical = ? LIMIT 1' with params [\"taha\"]:
SQLSTATE[42703]: Undefined column: 7 ERROR:  column t0.authcode does not exist
LINE 1: ...AS created_at_14, t0.updated_at AS updated_at_15, t0.authCod...
HINT:  Perhaps you meant to reference the column \"t0.authCode\". at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/AbstractPostgreSQLDriver.php:72, Doctrine\\DBAL\\Driver\\PDOException(code: 42703): SQLSTATE[42703]: Undefined column: 7 ERROR:  column t0.authcode does not exist
LINE 1: ...AS created_at_14, t0.updated_at AS updated_at_15, t0.authCod...
HINT:  Perhaps you meant to reference the column \"t0.authCode\". at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOStatement.php:107, PDOException(code: 42703): SQLSTATE[42703]: Undefined column: 7 ERROR:  column t0.authcode does not exist
LINE 1: ...AS created_at_14, t0.updated_at AS updated_at_15, t0.authCod...
HINT:  Perhaps you meant to reference the column \"t0.authCode\". at /home/taha/public/wallabag/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOStatement.php:105)"} []
[2021-06-13 15:04:36] security.DEBUG: Authentication failure, redirect triggered. {"failure_path":"/login"} []
```

So a missing column in table `wallabag_user`, apparently. Well, a missing column sounds like a smaller problem than `undefined table` which the other migration tool (`pgloader`) led to.
Let's try to work it.

In the MySQL database:
```
mysql> select * from wallabag_user;
+----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+---------------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+-------------------------+---------+
| id | username | username_canonical | email               | email_canonical     | enabled | salt                                        | password                                                                                 | last_login          | confirmation_token | password_requested_at | roles                                                  | name | created_at          | updated_at          | authCode | twoFactorAuthentication | trusted |
+----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+---------------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+-------------------------+---------+
|  1 | wallabag | wallabag           | webmaster@chepec.se | webmaster@chepec.se |       1 | gwNpnqJz.5x0zNaoWSYEJxMJTDYv8k99j1ty/1UDOW8 | Q5nySOz0f29ThfC2qkbhQiVbw/39geumDiKO99MvmKEBzZDTd5gw6U3UoQtRlEO73lxSuN/p3IPbiIRSBh3FDg== | 2021-06-05 18:19:13 | NULL               | NULL                  | a:2:{i:0;s:9:"ROLE_USER";i:1;s:16:"ROLE_SUPER_ADMIN";} | NULL | 2018-03-29 20:44:08 | 2021-06-05 18:19:13 |     NULL |                       0 | NULL    |
|  2 | taha     | taha               | taha@chepec.se      | taha@chepec.se      |       1 | eSpsapRnYyb5dLhsCUD/wvPrLcSVfgnmY4RcNJHdOpI | x/gLoLL8h5eVlXDdcBV/gqIWRdEV8kSjxWRF7WOqw/sFIcb2fhiesS4DfXZf5LKXWMjCeZSjkuGFj4SqhGEWpw== | 2021-06-10 22:41:15 | NULL               | NULL                  | a:1:{i:0;s:9:"ROLE_USER";}                             | NULL | 2018-03-29 21:01:59 | 2021-06-10 22:41:15 |     NULL |                       0 | NULL    |
+----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+---------------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+-------------------------+---------+
2 rows in set (0.00 sec)
```

Table columns and datatypes:
```
mysql> select column_name, data_type from information_schema.columns where table_schema = Database() and table_name = 'wallabag_user';
+-------------------------+-----------+
| column_name             | data_type |
+-------------------------+-----------+
| id                      | int       |
| username                | varchar   |
| username_canonical      | varchar   |
| email                   | varchar   |
| email_canonical         | varchar   |
| enabled                 | tinyint   |
| salt                    | varchar   |
| password                | varchar   |
| last_login              | datetime  |
| confirmation_token      | varchar   |
| password_requested_at   | datetime  |
| roles                   | longtext  |
| name                    | longtext  |
| created_at              | datetime  |
| updated_at              | datetime  |
| authCode                | int       |
| twoFactorAuthentication | tinyint   |
| trusted                 | longtext  |
+-------------------------+-----------+
18 rows in set (0.01 sec)
```

The contents of just the `authCode` column:
```
+----------+
| authCode |
+----------+
|     NULL |
|     NULL |
+----------+
```


In the migrated PGSQL database:
```
taha@taipei:~/public/wallabag
((2.3.8)) $ psql -d wallabag
psql (12.7 (Ubuntu 12.7-0ubuntu0.20.04.1))
Type "help" for help.

wallabag=> select * from wallabag_user;
 id | username | username_canonical |        email        |   email_canonical   | enabled |                    salt                     |                                         password                                         |     last_login      | confirmation_token | password_requested_at |                         roles                          | name |     created_at      |     updated_at      | authCode | twoFactorAuthentication | trusted 
----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+---------------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+-------------------------+---------
  1 | wallabag | wallabag           | webmaster@chepec.se | webmaster@chepec.se |       1 | gwNpnqJz.5x0zNaoWSYEJxMJTDYv8k99j1ty/1UDOW8 | Q5nySOz0f29ThfC2qkbhQiVbw/39geumDiKO99MvmKEBzZDTd5gw6U3UoQtRlEO73lxSuN/p3IPbiIRSBh3FDg== | 2021-06-05 18:19:13 |                    |                       | a:2:{i:0;s:9:"ROLE_USER";i:1;s:16:"ROLE_SUPER_ADMIN";} |      | 2018-03-29 20:44:08 | 2021-06-05 18:19:13 |          |                       0 | 
  2 | taha     | taha               | taha@chepec.se      | taha@chepec.se      |       1 | eSpsapRnYyb5dLhsCUD/wvPrLcSVfgnmY4RcNJHdOpI | x/gLoLL8h5eVlXDdcBV/gqIWRdEV8kSjxWRF7WOqw/sFIcb2fhiesS4DfXZf5LKXWMjCeZSjkuGFj4SqhGEWpw== | 2021-06-10 22:41:15 |                    |                       | a:1:{i:0;s:9:"ROLE_USER";}                             |      | 2018-03-29 21:01:59 | 2021-06-10 22:41:15 |          |                       0 | 
(2 rows)
```

Table columns and datatypes:
```
wallabag=> select column_name, data_type from information_schema.columns where table_name='wallabag_user';
       column_name       |          data_type          
-------------------------+-----------------------------
 id                      | integer
 username                | character varying
 username_canonical      | character varying
 email                   | character varying
 email_canonical         | character varying
 enabled                 | smallint
 salt                    | character varying
 password                | character varying
 last_login              | timestamp without time zone
 confirmation_token      | character varying
 password_requested_at   | timestamp without time zone
 roles                   | text
 name                    | text
 created_at              | timestamp without time zone
 updated_at              | timestamp without time zone
 authCode                | integer
 twoFactorAuthentication | smallint
 trusted                 | text
(18 rows)
```

Here's looking at the contents of just that column:
```
| authCode |
+----------+
|          |
|          |
```

In fact, it seems all cells with `NULL` values in the MySQL table have been replaced by empty cells in the Postgres table.
Is this perhaps the cause of the problem?

Why are these cells in an `integer` column empty string and not NULL?  
https://stackoverflow.com/questions/46477189/can-an-integer-column-be-null

+ https://github.com/AnatolyUss/nmig/issues/58
+ https://github.com/AnatolyUss/nmig/issues/47
+ https://github.com/AnatolyUss/nmig/issues/17
+ https://github.com/AnatolyUss/nmig/issues/4
+ https://github.com/AnatolyUss/nmig/pull/29


Let's try manually resetting these empty string values to `NULL`.

```
wallabag=> UPDATE wallabag_user SET "authCode" = NULL;
UPDATE 2
wallabag=> UPDATE wallabag_user SET trusted = NULL;
UPDATE 2
wallabag=> UPDATE wallabag_user SET name = NULL;
UPDATE 2
wallabag=> UPDATE wallabag_user SET password_requested_at = NULL;
UPDATE 2
wallabag=> UPDATE wallabag_user SET confirmation_token = NULL;
UPDATE 2
```

Hm, after running the above successful `UPDATE` statements, the table looks exactly the same as before 
(I was expecting the empty cells to say `NULL` instead).
This suggests that they were NULL already to start with, and that it's just a matter of how 
Postgresql *displays* NULL values.

This makes me think that perhaps the problem is actually the case-sensitive column name `authCode`.
There's also other columns with capital letters in the `wallabag_user` table: 
```
authCode
twoFactorAuthentication
```

+ https://stackoverflow.com/questions/20878932/are-postgresql-column-names-case-sensitive  
+ https://github.com/wallabag/wallabag/issues/2652

Let's rename them and see what happens:
```
wallabag=> alter table wallabag_user rename column "authCode" to authcode;
ALTER TABLE
wallabag=> alter table wallabag_user rename column "twoFactorAuthentication" to twofactorauthentication;
ALTER TABLE
```

Ok, that gets us around that error. Another one pops out, though. 
After logging in, we now get Wallabag's 500 error page:
```
500: Internal Server Error
An exception has been thrown during the rendering of a template (
    "An exception occurred while executing 
    'SELECT w0_.id AS id_0 FROM "wallabag_entry" w0_ WHERE w0_.user_id = ? AND w0_.is_archived = false GROUP BY w0_.id ORDER BY w0_.created_at DESC' 
    with params [2]: 
    SQLSTATE[42883]: Undefined function: 7 ERROR: operator does not exist: smallint = boolean LINE 1: ...y" 
    w0_ WHERE w0_.user_id = $1 AND w0_.is_archived = false GR... ^ 
    HINT: No operator matches the given name and argument types. You might need to add explicit type casts.").
```

+ https://mkyong.com/database/postgresql-error-operator-does-not-exist-smallint-character-varying-solution/




### Import mysqldump to PostgreSQL?

More than one source says that the compatibility argument of `mysqldump` *is not enough*.
http://dev.mysql.com/doc/refman/5.0/en/mysqldump.html#option_mysqldump_compatible

+ https://stackoverflow.com/questions/5417386/import-mysql-dump-to-postgresql-database
+ https://en.wikibooks.org/wiki/Converting_MySQL_to_PostgreSQL#Common_way_with_SQL_dump
+ https://serverfault.com/questions/633613/most-reliable-mariadb-to-postgres-dump-method



## Refs

+ https://www.codementor.io/@steve235/migrating-mysql-to-postgres-aevhkh3mn
+ https://stackoverflow.com/questions/51837224/mysqldump-compatible-mode-postgresql-is-not-working
+ https://dba.stackexchange.com/questions/15769/how-to-convert-a-mysql-database-to-postgresql
