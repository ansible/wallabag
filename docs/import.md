# Import user data from another Wallabag instance

+ [Document the steps for migrating between database backends (e.g., SQLite to MySQL/PGSQL) #4126, open issue since Nov 2019](https://github.com/wallabag/wallabag/issues/4126)


## Wallabag's web-based import (asynchronous, using Redis)

This role configures Redis.
On first login to the new Wallabag instance as admin, you should also manually enable Redis in the settings.

Then login as the desired user, and import the user's JSON file using the web-based GUI.
Appears to work (no complaints at least), but it appears to take ages.
Also, there appears to be no way to monitor the import job's progress.

In short, not very useful design.


## CLI-based importer

Luckily, Wallabag offers a CLI import command.  

I suggest running with the following args/flags (replace `<username> and `<path>`):
```
taha@shanghai:~/public/wallabag/2.4.2
$ bin/console wallabag:import <username> </path/to/backup.json> --env=prod --importer=v2 --disableContentUpdate
```

Remember to run the above command as `www-data` if necessary.

+ https://doc.wallabag.org/en/user/import/
+ https://doc.wallabag.org/en/admin/console_commands.html

This worked quickly, but some parts of some articles were still lost (e.g., endnotes).
It seems the import does not strictly respect the `--disableContentUpdate` flag? Or else the import process itself is lossy? Worrying in either case.



### Alright, let's try pgloader

+ https://github.com/dimitri/pgloader (last commit 2020-12-14, 3.4k stars, 158 open issues, 19 PRs)
+ https://pgloader.readthedocs.io/en/latest/

First, let's dump the SQL database on `armant` for safe-keeping.
(This is not needed to use pgloader, just being careful).

> Note that `armant` runs MySQL and not MariaDB.
> https://varhanik.net/check-mysql-installed/

Installation seems straight-forward. Latest release on Github is 3.6.2, and Ubuntu 20.04 repos has 3.6.1:
```
taha@shanghai:~/public/wallabag/2.4.2
$ apt-cache madison pgloader
  pgloader |    3.6.1-1 | http://archive.ubuntu.com/ubuntu focal/universe amd64 Packages
```

In short, we will run it on the container, after temporarily opening up `armant` to remote mysql connections.

Perhaps we just port forward the mysql port from `armant` to the LXD container? (No need to expose port).

+ Run this role on a freshly provisioned LXD container (should create empty psql database `wallabag`).

```
taha@shanghai:~/public/wallabag/2.4.2
$ sudo netstat -plnt
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      32107/sshd: /usr/sb 
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      18514/postgres      
```

Of course, the LXD container cannot connect to `armant`, so first, transfer the armant private key and add the relevant `ssh-config` snippet to the LXD container.
```
taha@shanghai:~
$ cat ~/.ssh/config 
IdentitiesOnly yes

Host armant
Hostname 192.168.1.106
Port 22
PreferredAuthentications publickey
IdentityFile /home/taha/.ssh/armant-taha
User taha
```

And with the ssh config in place, we can setup a tunnel between port 3306 on the container to port 3306 on `armant`:

```
$ ssh -f -N -L 3306:127.0.0.1:3306 armant
taha@shanghai:~
$ sudo netstat -plnt
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      21242/ssh           
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      32107/sshd: /usr/sb 
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      18514/postgres      
tcp6       0      0 ::1:3306                :::*                    LISTEN      21242/ssh           
```

+ `apt install pgloader`
+ Run pgloader (before logging in to Wallabag):

```
taha@shanghai:~
$ pgloader mysql://wallabag:<password>@localhost/wallabag postgresql:///wallabag
2021-06-05T15:51:03.011000Z LOG pgloader version "3.6.1"
2021-06-05T15:51:03.031000Z LOG Migrating from #<MYSQL-CONNECTION mysql://wallabag@localhost:3306/wallabag {1005D97503}>
2021-06-05T15:51:03.031000Z LOG Migrating into #<PGSQL-CONNECTION pgsql://taha@UNIX:5432/wallabag {1005EDAE23}>
2021-06-05T15:51:05.057000Z LOG report summary reset
                             table name     errors       rows      bytes      total time
---------------------------------------  ---------  ---------  ---------  --------------
                        fetch meta data          0         70                     0.080s
                         Create Schemas          0          0                     0.001s
                       Create SQL Types          0          0                     0.004s
                          Create tables          0         28                     0.107s
                         Set Table OIDs          0         14                     0.005s
---------------------------------------  ---------  ---------  ---------  --------------
            wallabag.migration_versions          0         36     0.5 kB          0.057s
           wallabag.wallabag_annotation          0          1     0.4 kB          0.054s
               wallabag.wallabag_config          0          2     0.1 kB          0.044s
 wallabag.wallabag_craue_config_setting          0         33     0.9 kB          0.092s
                wallabag.wallabag_entry          0       1450    33.0 MB          1.549s
            wallabag.wallabag_entry_tag          0        294     1.8 kB          0.025s
    wallabag.wallabag_oauth2_auth_codes          0          0                     0.019s
wallabag.wallabag_oauth2_refresh_tokens          0      11116     1.2 MB          0.150s
                  wallabag.wallabag_tag          0         31     0.7 kB          0.070s
                 wallabag.wallabag_user          0          2     0.6 kB          0.084s
 wallabag.wallabag_oauth2_access_tokens          0      11442     1.2 MB          0.154s
       wallabag.wallabag_oauth2_clients          0          3     0.7 kB          0.062s
      wallabag.wallabag_site_credential          0          0                     0.045s
         wallabag.wallabag_tagging_rule          0          0                     0.049s
---------------------------------------  ---------  ---------  ---------  --------------
                COPY Threads Completion          0          4                     1.646s
                         Create Indexes          0         41                     0.622s
                 Index Build Completion          0         41                     0.021s
                        Reset Sequences          0         11                     0.015s
                           Primary Keys          0         14                     0.004s
                    Create Foreign Keys          0         15                     0.018s
                        Create Triggers          0          0                     0.000s
                        Set Search Path          0          1                     0.001s
                       Install Comments          0          6                     0.000s
---------------------------------------  ---------  ---------  ---------  --------------
                      Total import time          ✓      24410    35.4 MB          2.327s
```

Database has been successfully imported. But Wallabag (web GUI) does not show anything.
No user taha, only wallabag. Cannot login as taha.

Is there perhaps some `bin/console` command one must run after importing a database?


Re-provisioned `shanghai` from scratch. I expect database wallabag to exist:
```
taha@shanghai:~
$ sudo -u postgres psql 
postgres=# \list
                              List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   
-----------+----------+----------+---------+---------+-----------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
 wallabag  | taha     | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/taha             +
           |          |          |         |         | taha=CTc/taha
(4 rows)

postgres=# \list+
                                                                List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   |  Size   | Tablespace |                Description                 
-----------+----------+----------+---------+---------+-----------------------+---------+------------+--------------------------------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 |                       | 7953 kB | pg_default | default administrative connection database
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7809 kB | pg_default | unmodifiable empty database
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7953 kB | pg_default | default template for new databases
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 wallabag  | taha     | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/taha             +| 9097 kB | pg_default | 
           |          |          |         |         | taha=CTc/taha         |         |            | 
(4 rows)
```

Ok, as expected. Hm, interesting, it's 9 MB already.

```
postgres=# \c wallabag
You are now connected to database "wallabag" as user "postgres".

wallabag=# \dt
                       List of relations
 Schema |                 Name                 | Type  | Owner 
--------+--------------------------------------+-------+-------
 public | migration_versions                   | table | taha
 public | wallabag_annotation                  | table | taha
 public | wallabag_config                      | table | taha
 public | wallabag_entry                       | table | taha
 public | wallabag_entry_tag                   | table | taha
 public | wallabag_ignore_origin_instance_rule | table | taha
 public | wallabag_ignore_origin_user_rule     | table | taha
 public | wallabag_internal_setting            | table | taha
 public | wallabag_oauth2_access_tokens        | table | taha
 public | wallabag_oauth2_auth_codes           | table | taha
 public | wallabag_oauth2_clients              | table | taha
 public | wallabag_oauth2_refresh_tokens       | table | taha
 public | wallabag_site_credential             | table | taha
 public | wallabag_tag                         | table | taha
 public | wallabag_tagging_rule                | table | taha
 public | wallabag_user                        | table | taha
(16 rows)
```

Let's have a look at the user table:
```
wallabag=# select * from wallabag_user;
 id | username | username_canonical |        email        |   email_canonical   | enabled |                    salt                     |                                         password                                         | last_login | confirmation_token | password_requested_at |                         roles                          | name |     created_at      |     updated_at      | authcode | emailtwofactor | googleauthenticatorsecret | backupcodes 
----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+----------------+---------------------------+-------------
  1 | wallabag | wallabag           | webmaster@chepec.se | webmaster@chepec.se | t       | <salt> | <password> |            |                    |                       | a:2:{i:0;s:9:"ROLE_USER";i:1;s:16:"ROLE_SUPER_ADMIN";} |      | 2021-06-05 12:17:38 | 2021-06-05 12:17:38 |          | f              |                           | 
(1 row)
```

And the entries table is empty:
```
wallabag=# select count(1) from wallabag_entry;
 count 
-------
     0
(1 row)
```

**Ok, now let's import our database from armant using pgloader and compare notes.**

```
postgres=# \list+
                                                                List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   |  Size   | Tablespace |                Description                 
-----------+----------+----------+---------+---------+-----------------------+---------+------------+--------------------------------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 |                       | 7953 kB | pg_default | default administrative connection database
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7809 kB | pg_default | unmodifiable empty database
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7953 kB | pg_default | default template for new databases
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 wallabag  | taha     | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/taha             +| 36 MB   | pg_default | 
           |          |          |         |         | taha=CTc/taha         |         |            | 
(4 rows)
```

Alright, the wallabag database has now grown to 36 MB.

I would expect the user table to contain two entries. Does it?
```
postgres=# \c wallabag;
wallabag=# select * from wallabag_user;
 id | username | username_canonical |        email        |   email_canonical   | enabled |                    salt                     |                                         password                                         | last_login | confirmation_token | password_requested_at |                         roles                          | name |     created_at      |     updated_at      | authcode | emailtwofactor | googleauthenticatorsecret | backupcodes 
----+----------+--------------------+---------------------+---------------------+---------+---------------------------------------------+------------------------------------------------------------------------------------------+------------+--------------------+-----------------------+--------------------------------------------------------+------+---------------------+---------------------+----------+----------------+---------------------------+-------------
  1 | wallabag | wallabag           | webmaster@chepec.se | webmaster@chepec.se | t       | jpSU/JqL.Ul8/lHX3JBO6olIloIX2.8XlHXEm9RLb98 | aVaSaVZSbQ/IMZaScDSZ8nW1oa1UL4vZH6RdZM+nF9Ag4tS06751BQIaz92X+xKD0n8UTuM7LKj8Vth4NYcIkg== |            |                    |                       | a:2:{i:0;s:9:"ROLE_USER";i:1;s:16:"ROLE_SUPER_ADMIN";} |      | 2021-06-05 12:17:38 | 2021-06-05 12:17:38 |          | f              |                           | 
(1 row)
```

Only one row. Weird.
Also, the entries table has zero rows. Very weird. Clearly, the migration from from armant MySQL to shanghai PostgreSQL did not work as intended, despite pgloader reporting success and the total database size appearing to match.


I'm thinking that perhaps the Wallabag installer initialises the database (9 MB after all) and that the fact that the database is not completely empty disturbs the pgloader migration in some subtle way.
**Let's drop the database and re-create it and try again.**

```
taha@shanghai:~
$ sudo -u postgres psql

postgres=# drop database wallabag;
DROP DATABASE

postgres=# \list
                              List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   
-----------+----------+----------+---------+---------+-----------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
(3 rows)
```

Create an empty database and set its owner and  access privileges as before
```
postgres=# create database wallabag;
CREATE DATABASE
postgres=# grant all privileges on database wallabag to taha;
GRANT
postgres=# alter database wallabag owner to taha;
ALTER DATABASE
postgres=# \list
                                                                List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   |  Size   | Tablespace |                Description                 
-----------+----------+----------+---------+---------+-----------------------+---------+------------+--------------------------------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 |                       | 7953 kB | pg_default | default administrative connection database
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7809 kB | pg_default | unmodifiable empty database
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +| 7953 kB | pg_default | default template for new databases
           |          |          |         |         | postgres=CTc/postgres |         |            | 
 wallabag  | taha     | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/taha             +| 7953 kB | pg_default | 
           |          |          |         |         | taha=CTc/taha         |         |            | 
(4 rows)
```

The database is still 8 MB. It seems this is the size of a fresh database.
Ok, let's try the `pgloader` migration again.

pgloader completed just as before.
Alright, user table now contains two rows. 
And the entries table contains 1450 rows. Much better.

Unfortunately, web page is white screen of death. Wallabag log indicates some serious SQL problems:
```
[2021-06-05 16:32:31] request.CRITICAL: Uncaught PHP Exception Twig\Error\RuntimeError: "An exception has been thrown during the rendering of a template ("An exception occurred while executing 'SELECT t0.value AS value_1, t0.name AS name_2, t0.section AS section_3 FROM "wallabag_internal_setting" t0 WHERE t0.name = ? LIMIT 1' with params ["matomo_enabled"]:  SQLSTATE[42P01]: Undefined table: 7 ERROR:  relation "wallabag_internal_setting" does not exist LINE 1: ..., t0.name AS name_2, t0.section AS section_3 FROM "wallabag_..^")." at /home/taha/public/wallabag/2.4.2/src/Wallabag/CoreBundle/Resources/views/base.html.twig line 78
```

Normally, I would have given up at this point. 
~~But the error appears localised to Matomo, so what if we disable Matomo integration on `armant` before trying again? Might be worth it.~~

And more importantly, we are importing Wallabag v2.3.8 into v2.4.2. That can't be good.
Let's have this Ansible role install 2.3.8 to simplify the migration.

Alright, with matching Wallabag versions, **migration from MySQL to PostgreSQL using `pgloader` works!**

Now, let's test-run an [upgrade](upgrade.md).



## Misc notes on PostgreSQL

```
taha@shanghai:~/public/wallabag/2.3.8
$ sudo -u postgres psql -c "\du"
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 taha      | Password valid until infinity                              | {}
```

Enter the wallabag database: `\c wallabag;`.

List all tables in the current database:
```
wallabag=> \dt
                    List of relations
 Schema |              Name              | Type  | Owner 
--------+--------------------------------+-------+-------
 public | migration_versions             | table | taha
 public | wallabag_annotation            | table | taha
 public | wallabag_config                | table | taha
 public | wallabag_craue_config_setting  | table | taha
 public | wallabag_entry                 | table | taha
 public | wallabag_entry_tag             | table | taha
 public | wallabag_oauth2_access_tokens  | table | taha
 public | wallabag_oauth2_auth_codes     | table | taha
 public | wallabag_oauth2_clients        | table | taha
 public | wallabag_oauth2_refresh_tokens | table | taha
 public | wallabag_tag                   | table | taha
 public | wallabag_tagging_rule          | table | taha
 public | wallabag_user                  | table | taha
(13 rows)
```

Show all columns/rows of a table:
```
wallabag=> select * from wallabag_config;
 id | user_id | theme | items_per_page | language | rss_token | rss_limit | reading_speed 
----+---------+-------+----------------+----------+-----------+-----------+---------------
(0 rows)
```
