# Calling handlers using their listen directive causes sudden death of notifying task

I was working on the wallabag handlers. 

At first, the error appeared to be triggered by this task in `main.yml`:
```
- name: "Change ownership to {{ wallabag_wwwuser }}"
  ansible.builtin.file: 
    path: "{{ wallabag_repo }}"
    recurse: yes
    owner: "{{ wallabag_wwwuser }}"
    group: "{{ wallabag_wwwuser }}"
  tags: upgrade-wallabag
  # cache needs to be cleared after upgrades
  notify: wallabag clear cache
```

but after nuking the container and starting over, I got:
```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
(master) $ ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit shanghai -vv 
[...]
TASK [mariadb : Copy my.cnf global MySQL configuration.] **************************************************************************
task path: /media/bay/taha/projects/ansible/roles/public/mariadb/tasks/configure.yml:13
ERROR! {{ ansible_env.HOME }}/public/wallabag: 'ansible_env' is undefined
```

```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
(master) $ ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit shanghai -vv 
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the controller starting with Ansible 2.12. Current version: 
3.6.9 (default, Jan 26 2021, 15:33:00) [GCC 8.4.0]. This feature will be removed from ansible-core in version 2.12. Deprecation 
warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.
ansible-playbook [core 2.11.1] 
  config file = /media/bay/taha/projects/ansible/playbooks/luxor/ansible.cfg
  configured module search path = ['/home/taha/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/taha/.local/lib/python3.6/site-packages/ansible
  ansible collection location = /home/taha/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/taha/.local/bin/ansible-playbook
  python version = 3.6.9 (default, Jan 26 2021, 15:33:00) [GCC 8.4.0]
  jinja version = 3.0.1
  libyaml = True
Using /media/bay/taha/projects/ansible/playbooks/luxor/ansible.cfg as config file
BECOME password: 
Vault password: 
```

Note that mariadb is the first active dependency of wallabag.

It seems then, that the first part of the playbook executes normally,
but as we get to the second part, fact gathering somehow fails?

```
- name: Testing pgsql and mysql containers
  hosts: lxd_testing
```

Ok, so what does manually collected facts contain?
```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
(master) $ ansible lxd_testing -m setup > facts-lxd_testing.txt
```

That contains everything you'd expect, including `ansible_env`.

I am starting to think that this is due to `playbook-containers.yml` containing more than one play.
The error always shows up at the beginning of the second play.
Perhaps fact gathering only works for the first play?



Very related issue? But old, if it is the problem, why am I seeing it now?
https://github.com/ansible/ansible/issues/14655


Now I am thinking this is all due to my recent reorganisation of the `tasks/` directory 
into subfolders. 
It seems Ansible does not allow subfolders, and this weird error could be a symptom of that.

I have not been able to find any doc that explicitly says subfolders inside the tasks dir
is disallowed, but this comment indicates that that is the case:
https://github.com/ansible/ansible/issues/20077#issuecomment-654749377 

+ I'm using `import_tasks`.
+ What about `include_role` with `tasks_from`? No, not useful for me.

There appears to be some support for subdirectories with `tasks_from`,
but nowhere does it say that we can organise our task directory into 
subdirectories.

So it seems I have come up against a quite poorly documented limitation of Ansible roles?

Except, after removing all subfolders in the `tasks/` directory, the exact same error
shows up again, at the very same place as before.

Removed empty lines in the `dependencies:` block in `meta/main.yml`.
No, no effect, same error.

What else?
Ok, I removed `redis_socket_path` from group_vars (it was a recent addition),
and it's already defined in the redis path defaults.

Rerunning playbook on a fresh container. Let's see what gives.
No, exactly the same error, at exactly the same place. 
I wasn't expecting that var to change anything, but what is going on?

Alright, now that we have flattened the tasks folder, let's also
add more explicit gather rules to the playbook.
I set `gather_facts: true` in both playbooks. Nope, still the same error.

Also, the playbook error shows on the MySQL task during first run on a fresh container,
but on subsequent playbook runs on the same container the error manifests later, 
at a PHP role task:

```
TASK [php : Ensure PHP packages are installed] ************************************************************************************
task path: /media/bay/taha/projects/ansible/roles/public/php/tasks/setup-Debian.yml:8
ERROR! {{ ansible_env.HOME }}/public/wallabag: 'ansible_env' is undefined
```

Same container, run playbook again, this time the error manifests later still:
```
TASK [php : Place PHP configuration file in place] ********************************************************************************
task path: /media/bay/taha/projects/ansible/roles/public/php/tasks/configure.yml:13
changed: [shanghai] => (item=/etc/php/7.3/cli) => {"ansible_loop_var": "item", "changed": true, "checksum": "d15c025d02b08b58bd25144a1bbd08bd5247faa4", "dest": "/etc/php/7.3/cli/php.ini", "gid": 0, "group": "root", "item": "/etc/php/7.3/cli", "md5sum": "6b265366a0edb35b4e4a71f1488a72f5", "mode": "0644", "owner": "root", "size": 3960, "src": "/home/taha/.ansible/tmp/ansible-tmp-1624895256.9401884-15344-218491963539925/source", "state": "file", "uid": 0}
ERROR! {{ ansible_env.HOME }}/public/wallabag: 'ansible_env' is undefined
```

Actually, upon an additional rerun of the playbook (without nuking the container)
the error manifests at a later point yet again:
```
TASK [php : Ensure APCu config file is present.] **********************************************************************************
task path: /media/bay/taha/projects/ansible/roles/public/php/tasks/configure-apcu.yml:20
changed: [shanghai] => (item=/etc/php/7.3/cli/conf.d) => {"ansible_loop_var": "item", "changed": true, "checksum": "a02d5034a2e693487a4c8d6a294988ae50ffb121", "dest": "/etc/php/7.3/cli/conf.d/20-apcu.ini", "gid": 0, "group": "root", "item": "/etc/php/7.3/cli/conf.d", "md5sum": "570144413b9f12c30c9f023770765d38", "mode": "0644", "owner": "root", "size": 66, "src": "/home/taha/.ansible/tmp/ansible-tmp-1624905757.2570553-27997-52064400361032/source", "state": "file", "uid": 0}
ERROR! {{ ansible_env.HOME }}/public/wallabag: 'ansible_env' is undefined
```

So I have no idea what could be causing that behaviour.


Ideas?
+ revert wallabag handlers
+ revert the entire wallabag role to pre-mysql state
+ reorganise containers playbook, using `import_playbook`

Alright, I reverted the `handlers/main.yml` file to commit 1173f58.
And lo and behold, the playbook ran without a hitch! 
I must say I'm astounded. Let's take a closer look at the diff for this file.

This worked:
```
- name: wallabag delete cache directory
  file:
    path: "{{ wallabag_repo }}/var/cache/"
    state: absent
  become: yes
  become_user: root
  listen: wallabag clear cache

- name: "bin/console cache:clear"
  command: "php bin/console cache:clear --env={{ wlbg_symfony_env }}"
  args:
    chdir: "{{ wallabag_repo }}"
  become: yes
  become_user: root
  listen: wallabag clear cache
```

And this appears to have been the cause of all this mayhem:
```
- name: wipe the wallabag cache directory
  ansible.builtin.file:
    path: "{{ wallabag_repo }}/var/cache/"
    state: absent
  become: yes
  become_user: root
  listen: wallabag clear cache

- name: "bin/console cache:clear"
  ansible.builtin.command: "php bin/console cache:clear --env={{ wlbg_symfony_env }}"
  args:
    chdir: "{{ wallabag_repo }}"
  become: yes
  become_user: root
  listen: wallabag clear cache

- name: "reset ownership of {{ wallabag_repo }}/var/cache/ to {{ wallabag_wwwuser }}"
  ansible.builtin.file:
    path: "{{ wallabag_repo }}/var/cache/"
    recurse: yes
    owner: "{{ wallabag_wwwuser }}"
    group: "{{ wallabag_wwwuser }}"
  become: yes
  become_user: root
  listen: wallabag clear cache
```


Ok, after a systematic investigation, I can conclude
+ the error only manifests when notifying the handlers using the `listen` directive.
+ notifying the handlers one-by-one using their `name` resolves the issue.
+ setting `error_on_missing_handler = False` in `ansible.cfg` had no effect on the issue.

I am very curious to understand why handlers in this role suddenly failed if we 
use their `listen` directives to call them. At the moment, I have no idea.
I will call the handlers using their names instead and move on for now.

A cursory search of the web returned nothing on the subject.


## Session info

```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
(master) $ ansible --version
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the controller starting with Ansible 2.12. Current version: 3.6.9 (default, Jan 26 2021, 15:33:00) [GCC 8.4.0]. This feature will be removed from ansible-core in version 2.12. Deprecation warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.
ansible [core 2.11.1] 
  config file = /media/bay/taha/projects/ansible/playbooks/luxor/ansible.cfg
  configured module search path = ['/home/taha/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/taha/.local/lib/python3.6/site-packages/ansible
  ansible collection location = /home/taha/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/taha/.local/bin/ansible
  python version = 3.6.9 (default, Jan 26 2021, 15:33:00) [GCC 8.4.0]
  jinja version = 3.0.1
  libyaml = True
```


## Refs

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/import_tasks_module.html
https://reddit.com/r/ansible/comments/eji7ie/should_we_consider_use_of_tasks_from_with_ansible/
