# Upgrading Wallabag

For now, this is a manual (not ansiblified) process.

As part of the migration of Wallabag from `armant` to LXD container `shanghai`, 
I wanted to confirm that upgrade from v2.3.8 to 2.4.2 would work.


## Upgrade from v2.3.8 to v2.4.2 (using precompiled release)

Following [these instructions](https://doc.wallabag.org/en/admin/upgrade.html).

First, we follow the steps for v2.2 to v2.3 upgrade:

+ Backup your SQL database and `parameters.yml`

```
taha@shanghai:~
# su - postgres
postgres@shanghai:~$ pg_dump -U postgres -d wallabag > wallabag.sql
```

Note that this writes the database to `/var/lib/postgresql/wallabag.sql`.  
https://severalnines.com/blog/using-pgdump-and-pgdumpall-backup-postgresql

```
taha@shanghai:~
$ cp public/wallabag/2.3.8/app/config/parameters.yml backups/
```

+ Download and extract the Wallabag v2.4.2 tarball

You can do this by running this playbook with `--tags "wlbg_download_unpack_tarball"`.

+ Replace your `parameters.yml` file, and make sure that you've made any [necessary changes](https://doc.wallabag.org/en/admin/parameters.html) to it.
+ Run `bin/console cache:clear --env=prod`

```
taha@shanghai:/home/taha/public/wallabag/2.4.2
$ bin/console cache:clear --env=prod

 // Clearing the cache for the prod environment with debug false                                                        

 [OK] Cache for the "prod" environment (debug=false) was successfully cleared.                                          
```

+ Empty Wallabag's `var/cache/` folder: `rm -rf var/cache/*`
+ Run [PostgreSQL queries for v2.3 to v2.4 upgrade](https://doc.wallabag.org/en/admin/query-upgrade-23-24.html):

```
taha@shanghai:~
$ psql -d wallabag
psql (12.7 (Ubuntu 12.7-0ubuntu0.20.04.1))
Type "help" for help.

wallabag=> 
```

then

```
ALTER TABLE wallabag_entry ADD archived_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;

ALTER TABLE "wallabag_user" ADD googleAuthenticatorSecret VARCHAR(191) DEFAULT NULL;
ALTER TABLE "wallabag_user" RENAME COLUMN twofactorauthentication TO emailTwoFactor;
ALTER TABLE "wallabag_user" DROP trusted;
ALTER TABLE "wallabag_user" ADD backupCodes TEXT DEFAULT NULL;

ALTER TABLE wallabag_site_credential ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;

ALTER TABLE wallabag_entry ADD hashed_url TEXT DEFAULT NULL;
CREATE INDEX hashed_url_user_id ON wallabag_entry (user_id, hashed_url);

ALTER TABLE "wallabag_config" RENAME COLUMN rss_token TO feed_token;
ALTER TABLE "wallabag_config" RENAME COLUMN rss_limit TO feed_limit;

ALTER TABLE "wallabag_oauth2_access_tokens" DROP CONSTRAINT FK_368A4209A76ED395;
ALTER TABLE "wallabag_oauth2_access_tokens" ADD CONSTRAINT FK_368A4209A76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "wallabag_oauth2_clients" DROP CONSTRAINT idx_user_oauth_client;
ALTER TABLE "wallabag_oauth2_clients" ADD CONSTRAINT FK_635D765EA76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "wallabag_oauth2_refresh_tokens" DROP CONSTRAINT FK_20C9FB24A76ED395;
ALTER TABLE "wallabag_oauth2_refresh_tokens" ADD CONSTRAINT FK_20C9FB24A76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "wallabag_oauth2_auth_codes" DROP CONSTRAINT FK_EE52E3FAA76ED395;
ALTER TABLE "wallabag_oauth2_auth_codes" ADD CONSTRAINT FK_EE52E3FAA76ED395 FOREIGN KEY (user_id) REFERENCES "wallabag_user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE wallabag_entry ADD given_url TEXT DEFAULT NULL;
ALTER TABLE wallabag_entry ADD hashed_given_url TEXT DEFAULT NULL;
CREATE INDEX hashed_given_url_user_id ON wallabag_entry (user_id, hashed_given_url);

UPDATE wallabag_config SET reading_speed = reading_speed*200;

ALTER TABLE "wallabag_entry" ALTER language TYPE VARCHAR(20);
CREATE INDEX user_language ON "wallabag_entry" (language, user_id);
CREATE INDEX user_archived ON "wallabag_entry" (user_id, is_archived, archived_at);
CREATE INDEX user_created ON "wallabag_entry" (user_id, created_at);
CREATE INDEX user_starred ON "wallabag_entry" (user_id, is_starred, starred_at);
CREATE INDEX tag_label ON "wallabag_tag" (label);
CREATE INDEX config_feed_token ON "wallabag_config" (feed_token);

ALTER TABLE "wallabag_craue_config_setting" RENAME TO "wallabag_internal_setting";

CREATE SEQUENCE ignore_origin_user_rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE ignore_origin_instance_rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE TABLE wallabag_ignore_origin_user_rule (id SERIAL NOT NULL, config_id INT NOT NULL, rule VARCHAR(255) NOT NULL, PRIMARY KEY(id));
CREATE INDEX idx_config ON wallabag_ignore_origin_user_rule (config_id);
CREATE TABLE wallabag_ignore_origin_instance_rule (id SERIAL NOT NULL, rule VARCHAR(255) NOT NULL, PRIMARY KEY(id));
ALTER TABLE wallabag_ignore_origin_user_rule ADD CONSTRAINT fk_config FOREIGN KEY (config_id) REFERENCES "wallabag_config" (id) NOT DEFERRABLE INITIALLY IMMEDIATE;

UPDATE wallabag_internal_setting SET name = 'matomo_enabled' where name = 'piwik_enabled';
UPDATE wallabag_internal_setting SET name = 'matomo_host' where name = 'piwik_host';
UPDATE wallabag_internal_setting SET name = 'matomo_site_id' where name = 'piwik_site_id';
```

Make sure the following parameters are included in your `parameters.yml`:

```
mailer_port
mailer_encryption
mailer_auth_mode
sentry_dsn
```

https://github.com/wallabag/wallabag/releases/tag/2.4.0  
Run `php bin/console --env=prod wallabag:generate-hashed-urls` which will generates a hash of URL for all saved entries (improve API search+++).

```
taha@shanghai:~/public/wallabag/2.4.2
$ sudo -u www-data php bin/console --env=prod wallabag:generate-hashed-urls
Generating hashed urls for "2" users
Processing user: wallabag
Generated hashed urls for user: wallabag
Processing user: taha
Generated hashed urls for user: taha
Finished generated hashed urls
```

Update the symlink from `/var/www/html/wallabag`!

Finally, clear the cache

```
taha@shanghai:~/public/wallabag/2.4.2
$ bin/console cache:clear --env=prod

 // Clearing the cache for the prod environment with debug false                                                        

 [OK] Cache for the "prod" environment (debug=false) was successfully cleared.                                          
```

and empty the cache folder (*after* effectively moving the Wallabag directory).  
https://github.com/wallabag/wallabag/issues/3809



### Lots of errors, appears Wallabag is still looking for 2.3.8 files

Not sure what's going on... the Apache error log:

```
[Sun Jun 06 02:13:37.059280 2021] [proxy_fcgi:error] [pid 2557:tid 140484924126976] [client 10.252.116.1:60714] AH01071: Got error 'PHP message: PHP Warning:  SessionHandler::read(): open(/home/taha/public/wallabag/2.3.8/var/sessions/prod/sess_324a210befadfc3a9030cf35718c1f5f, O_RDWR) failed: Permission denied (13) in /home/taha/public/wallabag/2.3.8/vendor/symfony/symfony/src/Symfony/Component/HttpFoundation/Session/Storage/Proxy/SessionHandlerProxy.php on line 62PHP message: PHP Warning:  session_write_close(): Failed to write session data with Symfony\\Component\\HttpFoundation\\Session\\Storage\\Handler\\NativeFileSessionHandler handler in /home/taha/public/wallabag/2.3.8/vendor/symfony/symfony/src/Symfony/Component/HttpFoundation/Session/Storage/NativeSessionStorage.php on line 240', referer: https://wlbg.chepec.se/js/routing?callback=fos.Router.setData
[Sun Jun 06 02:13:37.192252 2021] [proxy_fcgi:error] [pid 2557:tid 140484907341568] [client 10.252.116.1:60716] AH01071: Got error 'PHP message: PHP Fatal error:  Uncaught UnexpectedValueException: The stream or file "/home/taha/public/wallabag/2.3.8/var/logs/prod.log" could not be opened: failed to open stream: Permission denied in /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php:107\nStack trace:\n#0 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/AbstractProcessingHandler.php(39): Monolog\\Handler\\StreamHandler->write(Array)\n#1 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossedHandler.php(123): Monolog\\Handler\\AbstractProcessingHandler->handle(Array)\n#2 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(344): Monolog\\Handler\\FingersCrossedHandler->handle(Array)\n#3 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(735): Monolog\\Logger->addRecord(500, 'Uncaught PHP Ex...', Array)\n#4 /home/taha/public/wallabag/2.3.8/vendor/symfony/symfony/src/Symfony/Component/Ht...', referer: https://wlbg.chepec.se/wallassets/themes/_global/img/logo-wallabag.svg
[Sun Jun 06 02:13:37.666529 2021] [proxy_fcgi:error] [pid 2557:tid 140484479543040] [client 10.252.116.1:60718] AH01071: Got error 'PHP message: PHP Fatal error:  Uncaught UnexpectedValueException: The stream or file "/home/taha/public/wallabag/2.3.8/var/logs/prod.log" could not be opened: failed to open stream: Permission denied in /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php:107\nStack trace:\n#0 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/AbstractProcessingHandler.php(39): Monolog\\Handler\\StreamHandler->write(Array)\n#1 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossedHandler.php(123): Monolog\\Handler\\AbstractProcessingHandler->handle(Array)\n#2 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(344): Monolog\\Handler\\FingersCrossedHandler->handle(Array)\n#3 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(735): Monolog\\Logger->addRecord(500, 'Uncaught PHP Ex...', Array)\n#4 /home/taha/public/wallabag/2.3.8/vendor/symfony/symfony/src/Symfony/Component/Ht...', referer: https://wlbg.chepec.se/wallassets/themes/_global/img/logo-wallabag.svg
[Sun Jun 06 02:13:37.716002 2021] [proxy_fcgi:error] [pid 2558:tid 140484680869632] [client 10.252.116.1:60722] AH01071: Got error 'PHP message: PHP Fatal error:  Uncaught UnexpectedValueException: The stream or file "/home/taha/public/wallabag/2.3.8/var/logs/prod.log" could not be opened: failed to open stream: Permission denied in /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php:107\nStack trace:\n#0 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/AbstractProcessingHandler.php(39): Monolog\\Handler\\StreamHandler->write(Array)\n#1 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossedHandler.php(123): Monolog\\Handler\\AbstractProcessingHandler->handle(Array)\n#2 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(344): Monolog\\Handler\\FingersCrossedHandler->handle(Array)\n#3 /home/taha/public/wallabag/2.3.8/vendor/monolog/monolog/src/Monolog/Logger.php(735): Monolog\\Logger->addRecord(500, 'Uncaught PHP Ex...', Array)\n#4 /home/taha/public/wallabag/2.3.8/vendor/symfony/symfony/src/Symfony/Component/Ht...'
```

I have intentionally left the 2.3.8 folder owned by taha (2.4.2 is owned by www-data).
But why should Wallabag attempt to reach for anything in the 2.3.8 folder? That suggests that the upgrade is incomplete.

I thought perhaps we had missed to run the migrate command:
https://github.com/wallabag/wallabag/issues/2794#issuecomment-283917106  

```
taha@shanghai:~/public/wallabag/2.4.2
$ sudo -u www-data php bin/console doctrine:migrations:migrate --env=prod
                                                              
                    Application Migrations                    
                                                              

WARNING! You are about to execute a database migration that could result in schema changes and data loss. Are you sure you wish to continue? (y/n)y
Migrating up to 20200428072628 from 0

  ++ migrating 20160401000000

Migration 20160401000000 failed during Execution. Error An exception occurred while executing 'SELECT quote_ident(r.conname) as conname, pg_catalog.pg_get_constraintdef(r.oid, true) as condef
                  FROM pg_catalog.pg_constraint r
                  WHERE r.conrelid =
                  (
                      SELECT c.oid
                      FROM pg_catalog.pg_class c, pg_catalog.pg_namespace n
                      WHERE n.nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') AND c.relname = 'migration_versions' AND n.nspname = ANY(current_schemas(false)) AND n.oid = c.relnamespace
                  )
                  AND r.contype = 'f'':

SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression
19:26:19 ERROR     [console] Error thrown while running command "doctrine:migrations:migrate --env=prod". Message: "An exception occurred while executing 'SELECT quote_ident(r.conname) as conname, pg_catalog.pg_get_constraintdef(r.oid, true) as condef
                  FROM pg_catalog.pg_constraint r
                  WHERE r.conrelid =
                  (
                      SELECT c.oid
                      FROM pg_catalog.pg_class c, pg_catalog.pg_namespace n
                      WHERE n.nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') AND c.relname = 'migration_versions' AND n.nspname = ANY(current_schemas(false)) AND n.oid = c.relnamespace
                  )
                  AND r.contype = 'f'':

SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression" ["exception" => Doctrine\DBAL\Exception\DriverException { …},"command" => "doctrine:migrations:migrate --env=prod","message" => """  An exception occurred while executing 'SELECT quote_ident(r.conname) as conname, pg_catalog.pg_get_constraintdef(r.oid, true) as condef\n                    FROM pg_catalog.pg_constraint r\n                    WHERE r.conrelid =\n                    (\n                        SELECT c.oid\n                        FROM pg_catalog.pg_class c, pg_catalog.pg_namespace n\n                        WHERE n.nspname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') AND c.relname = 'migration_versions' AND n.nspname = ANY(current_schemas(false)) AND n.oid = c.relnamespace\n                    )\n                    AND r.contype = 'f'':\n  \n  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression  """]
            
  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression          

In PDOConnection.php line 91:

  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression  

In PDOConnection.php line 86:

  SQLSTATE[21000]: Cardinality violation: 7 ERROR:  more than one row returned by a subquery used as an expression  
```

So, that didn't work. I'm not sure it's supposed to work (the official upgrade guide does not mention it).


Does wallabag hard-code the path to wallabag in the database?
Does it actually say `/home/taha/public/wallabag/2.3.8` in the database, instead of `/var/www/html/wallabag`?

Well, a simply grep of the SQL file gave nothing, but I can confirm that this is the root of this issue.
Wallabag upgrades *do not* support keeping separate version folders and simply symlinking from `www-root`.
The upgraded version keeps looking for files in the old version tree. 

This fixed it:

```
taha@shanghai:~/public/wallabag
$ mv 2.3.8 2.3.8-old
$ mv 2.4.2 2.3.8
```

Very relevant issue: https://github.com/wallabag/wallabag/issues/3809
Related issue https://github.com/wallabag/wallabag/issues/3472
