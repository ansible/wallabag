During installation of Wallabag v2.4.2. 
Only showing the warnings (during `make install`).

I think we can safely ignore all of them.

> Note: this role no longer uses `make install`, instead installing Wallabag from the 
> precompiled tarball (making this note irrelevant).


```
Package doctrine/doctrine-cache-bundle is abandoned, you should avoid using it. No replacement was suggested.
Package doctrine/reflection is abandoned, you should avoid using it. Use roave/better-reflection instead.
Package guzzlehttp/ringphp is abandoned, you should avoid using it. No replacement was suggested.
Package guzzlehttp/streams is abandoned, you should avoid using it. No replacement was suggested.
Package liip/theme-bundle is abandoned, you should avoid using it. Use sylius/theme-bundle instead.
Package sensio/distribution-bundle is abandoned, you should avoid using it. No replacement was suggested.
Package sensiolabs/security-checker is abandoned, you should avoid using it. Use https://github.com/fabpot/local-php-security-checker instead.
Package twig/extensions is abandoned, you should avoid using it. No replacement was suggested.
Class SimpleHtmlDom\simple_html_dom_node located in ./vendor/mgargano/simplehtmldom/src/simple_html_dom.php does not comply with psr-0 autoloading standard. Skipping.
Class SimpleHtmlDom\simple_html_dom located in ./vendor/mgargano/simplehtmldom/src/simple_html_dom.php does not comply with psr-0 autoloading standard. Skipping.
Class Sensio\Bundle\FrameworkExtraBundle\Tests\DependencyInjection\AddExpressionLanguageProvidersPassTest located in ./vendor/sensio/framework-extra-bundle/Tests/DependencyInjection/Compiler/AddExpressionLanguageProvidersPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Sensio\Bundle\FrameworkExtraBundle\Tests\DependencyInjection\AddParamConverterPassTest located in ./vendor/sensio/framework-extra-bundle/Tests/DependencyInjection/Compiler/AddParamConverterPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Sensio\Bundle\FrameworkExtraBundle\Tests\Request\ParamConverter\ArgumentNameConverterTest located in ./vendor/sensio/framework-extra-bundle/Tests/Request/ArgumentNameConverterTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Nelmio\ApiDocBundle\Tests\Extractor\SensioFrameworkExtraHandlerTest located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Extractor/Handler/SensioFrameworkExtraHandlerTest.php does not comply with psr-0 autoloading standard. Skipping.
Class Nelmio\ApiDocBundle\Tests\Extractor\FosRestHandlerTest located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Extractor/Handler/FosRestHandlerTest.php does not comply with psr-0 autoloading standard. Skipping.
Class Nelmio\ApiDocBundle\Tests\Functional\AppKernel located in ./vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Tests/Fixtures/app/AppKernel.php does not comply with psr-0 autoloading standard. Skipping.
Class Liip\ThemeBundle\Tests\DependencyInjection\ThemeCompilerPassTest located in ./vendor/liip/theme-bundle/Tests/DependencyInjection/Compiler/ThemeCompilerPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Liip\ThemeBundle\Tests\DependencyInjection\AsseticTwigFormulaPassTest located in ./vendor/liip/theme-bundle/Tests/DependencyInjection/Compiler/AsseticTwigFormulaPassTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Liip\ThemeBundle\Tests\EventListener\UseCaseTest located in ./vendor/liip/theme-bundle/Tests/UseCaseTest.php does not comply with psr-4 autoloading standard. Skipping.
Class Doctrine\Bundle\MigrationsBundle\Tests\DependencyInjection\DoctrineCommandTest located in ./vendor/doctrine/doctrine-migrations-bundle/Tests/Command/DoctrineCommandTest.php does not comply with psr-4 autoloading standard. Skipping.
```

