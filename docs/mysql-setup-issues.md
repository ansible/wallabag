# MariaDB root auth issues

Because of my work testing Wallabag on an LXC container,
I have rewritten the `mariadb` role from scratch.

Before this, the mariadb service on the container was broken.
Now, it's active (running) but I still have a **warning** about
`Access denied for user root@localhost`.
And the wallabag instance is still not reachable, although 
the error shows as 404 and should be unrelated to the database
(but I have not been able to pin-point any reason for a 404).

Here's how it looks:

```
taha@shanghai:~
((2.3.8)) $ sudo systemctl status mysqld.service 
● mariadb.service - MariaDB 10.4.20 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
    Drop-In: /etc/systemd/system/mariadb.service.d
             └─migrated-from-my.cnf-settings.conf
     Active: active (running) since Sun 2021-07-11 04:34:42 CEST; 1h 42min ago
       Docs: man:mysqld(8)
             https://mariadb.com/kb/en/library/systemd/
   Main PID: 17638 (mysqld)
     Status: "Taking your SQL requests now..."
      Tasks: 32 (limit: 4915)
     Memory: 101.3M
     CGroup: /system.slice/mariadb.service
             └─17638 /usr/sbin/mysqld

Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: information_schema
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: mysql
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: performance_schema
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: Phase 6/7: Checking and upgrading tables
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: Processing databases
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: information_schema
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: performance_schema
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: Phase 7/7: Running 'FLUSH PRIVILEGES'
Jul 11 04:34:46 shanghai /etc/mysql/debian-start[17678]: OK
Jul 11 04:46:36 shanghai mysqld[17638]: 2021-07-11  4:46:36 53 [Warning] Access denied for user 'root'@'localhost'
```

Note that the denied access warning occurs about 10 minutes after mariadb is installed on the container.
Making me think that perhaps something in the Wallabag role sets off the warning.

I changed auth method in the mysql tasks to `login_unix_socket`, 
which appears to have resolved the warning about access denied.

Still get 404 for the wallabag instance, though. Don't know what that's about.
Figured it out - the handler for apache reload was not (still isn't) triggering
as often as expected. For now, this is easily fixed by manually issuing
`systemctl reload apache2`.
