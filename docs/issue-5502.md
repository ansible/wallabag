#  Migrating Wallabag from mysql to postgresql, missing relations error #5502

https://github.com/wallabag/wallabag/issues/5502

My actions in response to j0k3r's response
https://github.com/wallabag/wallabag/issues/5502#issuecomment-1006871510


Re-created `taipei` from scratch (ran the containers playbook with `--limit taipei`).
This migrated all the Wallabag data from `shanghai` to `taipei`. OK.

Logged in to `taipei`.

Inspected the Wallabag PostgreSQL database on `taipei` (as `taha`, not as `postgres`!):
```
taha@taipei:~
$ psql 
psql (14.1 (Ubuntu 14.1-2.pgdg20.04+1), server 12.9 (Ubuntu 12.9-2.pgdg20.04+1))
Type "help" for help.

taha=> \c wallabag

wallabag=> select * from wallabag_entry_tag;
 entry_id | tag_id 
----------+--------
        2 |      1
        2 |      2
        4 |      1
        4 |      2
        4 |      3
(5 rows)

wallabag=> \d wallabag_entry_tag;
        Table "wallabag.wallabag_entry_tag"
  Column  |  Type  | Collation | Nullable | Default 
----------+--------+-----------+----------+---------
 entry_id | bigint |           | not null | 
 tag_id   | bigint |           | not null | 
Indexes:
    "idx_16425_primary" PRIMARY KEY, btree (entry_id, tag_id)
    "idx_16425_idx_c9f0dd7cba364942" btree (entry_id)
    "idx_16425_idx_c9f0dd7cbad26311" btree (tag_id)
Foreign-key constraints:
    "fk_entry_tag_entry" FOREIGN KEY (entry_id) REFERENCES wallabag_entry(id) ON UPDATE RESTRICT ON DELETE CASCADE
    "fk_entry_tag_tag" FOREIGN KEY (tag_id) REFERENCES wallabag_tag(id) ON UPDATE RESTRICT ON DELETE CASCADE
```

Let's try the `CREATE SEQUENCE` commands that
[j0k3r suggested](https://github.com/wallabag/wallabag/issues/5502#issuecomment-1006871510),
and set the start value to the largest existing value plus 1,
except for `entry_id`, which needs `+2` (the largest `entry_id` in `wallabag_entry_tag`
is actually one less than the number of articles..., what's that about?):
```
wallabag=# CREATE SEQUENCE "entry_id_seq" INCREMENT BY 1 MINVALUE 1 START 6;
CREATE SEQUENCE
wallabag=# CREATE SEQUENCE "tag_id_seq" INCREMENT BY 1 MINVALUE 1 START 4;
CREATE SEQUENCE
wallabag=> \d wallabag_entry_tag;
        Table "wallabag.wallabag_entry_tag"
  Column  |  Type  | Collation | Nullable | Default 
----------+--------+-----------+----------+---------
 entry_id | bigint |           | not null | 
 tag_id   | bigint |           | not null | 
Indexes:
    "idx_16425_primary" PRIMARY KEY, btree (entry_id, tag_id)
    "idx_16425_idx_c9f0dd7cba364942" btree (entry_id)
    "idx_16425_idx_c9f0dd7cbad26311" btree (tag_id)
Foreign-key constraints:
    "fk_entry_tag_entry" FOREIGN KEY (entry_id) REFERENCES wallabag_entry(id) ON UPDATE RESTRICT ON DELETE CASCADE
    "fk_entry_tag_tag" FOREIGN KEY (tag_id) REFERENCES wallabag_tag(id) ON UPDATE RESTRICT ON DELETE CASCADE
```

I wiped the Wallabag cache, cleared its Symphony cache, and 
restarted the proxy Apache webserver (last step probably unnecessary).
```
taha@taipei:~
$ sudo rm -rf public/wallabag/var/cache
taha@taipei:~/public/wallabag
$ sudo -u www-data php bin/console cache:clear --env=prod
$ sudo systemctl restart apache2.service
```

Tried to add a new tag to an article. Worked!

Tried to add a new article. Worked when start value was `+2`!

Error when start value was `+1`:
```
500: Internal Server Error An exception occurred while executing 'INSERT INTO "wallabag_entry"
SQLSTATE[23505]: Unique violation: 7
ERROR: duplicate key value violates unique constraint "idx_16415_primary"
DETAIL: Key (id)=(5) already exists.
```

So that error is pretty unambiguous. Key id 5 already exists. Also makes sense, 
in that our Wallabag has 5 existing articles. What's weird is that the `entry_id`
column in `wallabag_entry_tag` table only goes up to 4...

