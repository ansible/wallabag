# Wallabag

This Ansible role installs Wallabag, either from precompiled archives or from 
source.

This role can handle upgrades (except for the subsequent SQL migration),
tested for Wallabag v2.3.8 (installed from source to v2.4.2 (installed
from source).

I originally wrote this role to help me migrate my own Wallabag instance 
from a bare-metal host running Ubuntu 18.04 and MySQL to an LXD container
running Ubuntu 20.04 and PostgreSQL.

The MySQL to PostgreSQL migration is handled by the excellent
[PGLoader tool](https://github.com/dimitri/pgloader) by Dimitri Fontaine.


## Example playbook

To run this role, I suggest the following `playbook.yml`:

```
---

- name: Setup Wallabag on a fresh Ubuntu 20.04 VPS or LXC container
  hosts: lxd_containers

  tasks:
    - name: "https://codeberg.org/ansible/common"
      import_role:
        name: common
      become: true
    - name: "https://codeberg.org/ansible/common-systools"
      import_role:
        name: common-systools
      become: true
    - name: "https://codeberg.org/ansible/locales"
      import_role:
        name: locales
      become: true
    - name: "https://codeberg.org/ansible/dotfiles"
      import_role:
        name: dotfiles
    - name: "https://codeberg.org/ansible/ssh"
      import_role:
        name: ssh
      become: true
    - name: "https://codeberg.org/ansible/python3"
      import_role:
        name: python3
      become: true
    - name: "https://codeberg.org/ansible/wallabag"
      import_role:
        name: wallabag
      become: true
```

with the following `hosts.yml` file
(my use of `ansible_host` works because the hostname is defined in my
`~/.ssh/config` file):

```
all:
  vars: 
    ansible_python_interpreter: /usr/bin/python3
lxd_containers:
  hosts:
    taipei:
      ansible_host: taipei
```

and the following `ansible.cfg`:

```
[defaults]
inventory = ./hosts.yml
roles_path = {{ playbook_dir }}/roles

[privilege_escalation]
become_method = sudo

[ssh_connection]
pipelining = True
ssh_args = -o ServerAliveInterval=30 -o ServerAliveCountMax=3
```

You don't need *all* the roles in the example playbook above (e.g., `locales` and `dotfiles` roles
are probably quite specific to my own setup), but don't forget to place all the roles you do import
in your playbook in the `roles_path` you configured in your `ansible.cfg`.

Note that this role (`wallabag`) also **depends** on the following roles which need not be
imported in the playbook (because they are specified as dependencies in this role's `meta`
folder), but you must make sure they too are available from the `roles_path`:

+ [postgres](https://codeberg.org/ansible/postgres)
+ [mariadb](https://codeberg.org/ansible/mariadb) (only necessary if you intend to use MariaDB backend for Wallabag)
+ [composer](https://codeberg.org/ansible/composer) (only necessary if you intend to install Wallabag from source)
+ [nodejs](https://codeberg.org/ansible/nodejs) (not necessary, only used during testing of this role)
+ [apache](https://codeberg.org/ansible/apache)
+ [redis](https://codeberg.org/ansible/redis)

I suggest you create a directory structure for your playbook looking like this:

```
.
├── ansible.cfg
├── hosts.yml
├── playbook.yml
└── roles
    ├── common
    ├── wallabag
    └── ...
```


## Internal settings

Wallabag offers the instance administrator some settings only from the web interface.


### Download images locally

I have set it to `1`. I like the idea of saving not only the article text but also the images locally.

> Once this feature is activated, the articles pictures will be downloaded in 
> the `web/assets/images` folder of your Wallabag instance. The path of pictures
> in articles will also be updated to the path of pictures which are on your instance.
> If you want GIFs to keep animation, install `imagick` PHP extension.
> https://doc.wallabag.org/en/admin/internal_settings.html#download-images-locally


## Handle errors during fetching by adjusting site config?

Should I incorporate site config fixes in this role?

+ https://doc.wallabag.org/en/user/errors_during_fetching.html#creationupdate-of-a-site-configuration-file
+ https://f43.me/feed/test
+ http://siteconfig.fivefilters.org


## How I used this role to migrate (mysql -> pgsql) and upgrade Wallabag (2.3.8 -> 2.4.2)

> This is the short version :-)
> For more details, see the detailed notes in the folder `docs/wallabag-migration-upgrade/`.

First, I setup the LXD server on my bare-metal host and created a fresh LXD container
using [my `lxd-server` role](https://codeberg.org/ansible/lxd-server):
```
ansible-playbook playbook-host.yml --ask-become-pass --ask-vault-pass --tags "lxd-server"
```

Then I provisioned the LXD container for Wallabag using this role
(with `wlbg.version: 2.3.8` and `import_sql_database: true`):
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass
```

This run does all the heavy lifting of migrating our database from mysql to postgresql,
and runs the necessary `CREATE SEQUENCE` sql commands to recreate the otherwise missing
index values of these sequences (I'm still uncertain *why* the SQL migration script doesn't
handle this...).

At this point, the Wallabag instance on the container should be a working replica
of our original instance. Huzzah!

Next, upgrade to the latest Wallabag release v2.4.2
(now with `wlbg.version: 2.4.2` and `import_sql_database: false`):
```
ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --tags "upgrade-wallabag"
```

Finally, a manual step is necessary, namely to run 
[Wallabag's upgrade PostgreSQL queries](https://doc.wallabag.org/en/admin/query-upgrade-23-24.html).

Mission accomplished!

I would like to thank Wallabag maintainer Jérémy Benoist for his help at
[a critical juncture during this project](https://github.com/wallabag/wallabag/issues/5502),
and the author and developers of the [`pgloader` tool](https://github.com/dimitri/pgloader)
for their work, without which this project would never have come to completion.


## Some notes on other Wallabag installation options

### Wallabag with Docker Compose

+ https://hub.docker.com/r/wallabag/wallabag/
+ https://github.com/wallabag/docker
+ https://github.com/wallabag/docker/blob/master/root/etc/ansible/entrypoint.yml

The Wallabag Docker image is in effect a wrapper around an Ansible playbook.


### Ansible playbooks/roles for installation of Wallabag

+ https://github.com/davestephens/ansible-nas/ Installs Wallabag using Docker container.
+ https://github.com/guillaumebriday/selfhosted-services Installs Wallabag using Docker container.
+ https://github.com/ovv/ansible-role-wallabag Installs Wallabag using `bin/console` command, like my role.


## Refs

+ https://blog.braincoke.fr/server/install-wallabag-on-your-server/
+ https://www.linuxbabe.com/ubuntu/install-wallabag-ubuntu-16-04
+ https://gitter.im/wallabag/wallabag
