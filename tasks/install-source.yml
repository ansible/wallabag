---

- name: Install Wallabag from source
  tags: 
    - wlbg_install_source
    - upgrade-wallabag
  block:

    - debug:
        msg: "Entering install-source.yml"

    # it's not necessary to create the directory first
    # note: our use of force ensures any existing wallabag repo is overwritten
    - name: "Checkout Wallabag release v{{ wallabag_version }}"
      ansible.builtin.git:
        repo: https://github.com/wallabag/wallabag.git
        dest: "{{ wallabag_repo }}"
        version: "{{ wallabag_version }}"
        force: yes
    
    # subsequent tasks in this file expect wallabag repo to be owned by our primary user
    - name: "Change ownership of wallabag repo to {{ ansible_env.USER }}"
      ansible.builtin.file:
        path: "{{ wallabag_repo }}"
        state: directory
        recurse: yes # otherwise only the folder itself is affected
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
    
    # configure parameters.yml
    # Beware, passwords must be surrounded by single quotes (') in parameters.yml
    # this file had -rw-rw-r-- permissions by default, which seems wrong given it contains secrets and passwords
    # Every time you change something in the parameters.yml, you have to clear the cache so the app will rebuild it
    # https://github.com/wallabag/wallabag/issues/4451#issuecomment-648087317
    - name: Configure parameters.yml
      ansible.builtin.template:
        src: parameters.yml.j2
        dest: "{{ wallabag_repo }}/app/config/parameters.yml"
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
        mode: 0640
      notify: 
        - "Delete the wallabag cache directory"
        - "Clear the wallabag symfony cache"
        - "Reset ownership of wallabag directory"
    
    - name: Install Wallabag from source
      become: true
      become_user: "{{ ansible_env.USER }}"
      block:
    
        - name: composer install (this takes a while...)
          ansible.builtin.command: "composer install --no-dev -o --prefer-dist"
          environment:
            SYMFONY_ENV: "{{ wlbg.symfony_env }}"
          args:
            chdir: "{{ wallabag_repo }}"
        
        - name: "php bin/console wallabag install"
          ansible.builtin.command: >
            php bin/console
            wallabag:install
            --env={{ wlbg.symfony_env }}
            --no-interaction
          args:
            chdir: "{{ wallabag_repo }}"
    
        # there's no need to be parsimonious with this command, I see no harm in resetting
        # https://doc.wallabag.org/en/admin/console_commands.html
        - name: Reset password for the wallabag admin user
          ansible.builtin.command: >
            php bin/console
            fos:user:change-password
            {{ wallabag_admin_username }} '{{ wallabag_admin_password }}'
            --env={{ wlbg.symfony_env }}
          args:
            chdir: "{{ wallabag_repo }}"
          no_log: true
      # END OF BLOCK
  # END OF BLOCK
