---

# The ansible mysql modules require the pip-module PyMySQL to be installed on the node,
# for the user that Ansible executes the module as (which is root in this case).
# Make sure that PyMySQL is installed for root on the Ansible remote 
# (this is handled by my python3 and mariadb roles)

# check what mysql databases exist
# https://docs.ansible.com/ansible/latest/collections/community/mysql/mysql_info_module.html
- name: Collect info on all existing mysql databases
  community.mysql.mysql_info:
    # check_implicit_admin: yes # not a supported parameter for mysql_info
    login_unix_socket: "{{ mariadb_socket }}"
    filter:
      - "databases"
  register: wlbg_mysql_info

# If mysql database {{ db_name }} does not exist, create it
# https://docs.ansible.com/ansible/latest/modules/mysql_db_module.html#mysql-db-module
- name: Create empty Wallabag database
  community.mysql.mysql_db:
    check_implicit_admin: yes
    login_unix_socket: "{{ mariadb_socket }}"
    name: "{{ db_name }}"
    state: present
    login_host: "{{ wlbg_database_host }}"
    login_port: "{{ wlbg_database_port }}"
  no_log: true
  when: "db_name not in wlbg_mysql_info.databases"
  register: wlbg_mysql_database

# create mysql user and grant it permissions to wallabag database
- name: "Create mysql user {{ db_user }} and grant it permissions on database {{ db_name }}"
  community.mysql.mysql_user:
    check_implicit_admin: yes
    login_unix_socket: "{{ mariadb_socket }}"
    name: "{{ db_user }}"
    state: present
    password: "{{ wlbg_database_pwd }}"
    login_host: "{{ wlbg_database_host }}"
    login_port: "{{ wlbg_database_port }}"
    priv: "{{ db_name }}.*:ALL"
  no_log: true
